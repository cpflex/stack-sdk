# CP-Flex Cloud Stack Services Open API definitions
Open API specifications are provided for the CP-Flex cloud services.  Please visit http://global.cpflex.tech/console/documents.html for the latest API definitions and interactive explorer.

---
*Copyright 2019-2020, Codepoint Technologies, Inc.* 
*All Rights Reserved*


