# CP-Flex� Translation API 

### Contents of this document

* [Overview](##Overview)
* [Reference Documentation](##Reference-Documentation)
* [Decoding Received Data Packets](##Decoding-Received-Data-Packets)
* [Encoding Transmission Data Packets](##Encoding-Transmission-Packets)
* [Complete Translation API Structure](##Complete-Translation-API-Structure)

## Overview
The translation API enables solution providers to communicate with CP-Flex enabled tags.  All messages being 
communicated between the cloud application services and the CP-FLex Service must be encoded into a 
highly compact and secure format sized appropriately for the communication medium.  For example, version 1
of the CP-Flex enabled tag is restricted to 51 bytes by the limited communication packet size over the LoRaWAN network.   The Translation
API takes care of these details as well as handling location calculation and translation of encoded data into 
the namespace of the specific flex script application deployed on the tag device.

The translation API can support both JSON and binary XMF data formats over HTTP(S) in communicating with the Tracking Solution.  
All communication with the tag uses a proprietary binary protocol and requires translation it can be used by the tracking service. 

For version 1.0 of this SDK, only the JSON format is documented.  Solution providers should contact CP-Flex Service reprensentatives for more
information to access the more compact (transport efficient) binary formats.

In general, the encode and decode web service methods interact with the user through five
standard message structures.

![message classes](./images/translation_messages_class.png)

Message is the base structure common to all messages containing msgtype and sent DateTime.  FlexMessage
is the base structure common to all Flex messages, which define 'nids' that are named identifiers
associated with the Flex applicaition OCM specification.  The key messages are defined as follows:

- **LocationReport** - Typically provided every time packet data is decoded, will provide the last
reported location of the device.  Has an 'updated' boolean field that indicates if the location was updated
as part of the packet data received.
- **InvalidMsg** - This will be returned to the solution provider whenever a specific packet
or message is deemed invalid.  It provides more details in edition to basic error information.
- **Int32Msg** - A 32bit integer array message conforming to the Flex OCM spec.   This message
sends 1 or more integer / keyed values to the tag.  This message is typically used to 
send receive commands / state change data that is easily represented as integer enumerations. When
defining OCM specificications Int32Msg is encouraged where applicable as it can be highly compacted for
transmission.
- **KeyValueMsg** - A set of key value pairs conforming to the Flex OCM spec.  Use the KeyValueMsg
to transmit/receive multi-valued complex data, where small transmission size is not the highest 
priority.  The KV pair supports encoded transmission of string, integer values and raw binary data.

- **UnencodedMsg** - This message is used to send raw binary, json, or text data to the tag.  Data contained in
this message are not encoded/decoded using the OCM spec and only require a nid specifier. This
message is not typically recommended as it does not get encoded for compact transmission.


## Reference Documentation
The reference for all the types can be found by clicking the following:
- [AsciiDoc Reference](./asciidoc/api/index_hierarchy.adoc)
- [HTML Reference](./html/api/index_hierarchy.html)


## Decoding Received Data Packets 
The general sequence for receiving data from a tag is shown in the following diagram.

![Translation Decode](./images/translation_decode_seq.png)

The tag posts a sequence of data packets (1 or more) typically in response to some event.  These packets are 
forward to the Tracking Solution service by the LoRa Network implementation.  The Tracking solution performs
rudimentary validation of the received packet data and then passes to the Decode API on the CP-Flex translation 
API.  

Depending on the data received, the service may calculate an updated position.  Following this, 
the service decodes any additional event information, verifying it matches the Flex Object Configuration Model (OCM) 
specification for the provisioned device.  Encoded event data is mapped to the OCM message specifications,
to provide additional context and meaning for the tracking solution without requiring the Tracking Solution to 
manage potential differences in devices and deployed OCM compliant Flex application scripts.

The decoded data is returned to the Tracking Solution for further processing.

### Decode Example 
The following example shows demonstrates receiving LoRa Tag data from the EasyLinkin Linkware cloud service, which
provides LoRaWAN communications with the LoRa tag.   The Linkware cloud service will push one or more packets
of data to the Tracking solution.  In this case, the tracking solution receives the following JSON Message

	[
	  {  
	     "mac": "flex_test1",  
	     "appeui": "2c26c50124194000", 
         "last_update_time": "20190808180726",  
         "data_type": "2", 
         "data": "01826cf4482ccb010efcecda84ce7a37199182217536440206083101000000032540020440410941010543046e140000410941", 
         "reserver": {"code": 0}
	  },
	  {
	     "mac": "flex_test1",  
         "appeui": "2c26c50124194000", 
         "last_update_time": "20190808180726",  
         "data_type": "2", 
         "data": "01016cf44813e501064304b80b00004109410103430458020000", 
         "reserver": {"code": 0}
	  }
	]

The messages contains two packets of data from the device with mac "flex_test1" (this is not a real devEUI).
The data is provided in hex format, which must be recoded as base64, then it can be copied directly to
the translation decode JSON message as follows:

	{
	  "confidence": 0.6826890110969543,
	  "devid": "flex_test1",
	  "typeShape": [ 31 ],
	  "upload": [
		{
		  "dateTransport": "2019-08-08T18:07:26.935163020Z",
		  "gateways": [],
		  "payload": "AYLeZEwsywEO/OzahM56NxmRgiF1NkQCBggxAQAAAAMlQAIEQEEJQQEFQwRuFAAAQQlB"
		},
		{
		  "dateTransport": "2019-08-08T18:07:26.935163020Z",
		  "gateways": [],
		  "payload": "AQHeZEwT5QEGQwS4CwAAQQlBAQNDBFgCAAA="
		}
	  ]
	}

This message is posted to the Decode translation service which responds with the following
content.

	{
	  "devid": "flex_test1",
	  "download": [],
	  "messages": [
		{
		  "category": "alert",
		  "msgtype": "Int32Msg",
		  "nid": "PTrk.Msg.Emergency",
		  "priority": "critical",
		  "sent": "2019-08-08T18:07:26.000000Z",
		  "svals": [ "PTrk.Msg.Emergency.Enabled" ],
		  "vals": [ 1 ]
		},
		{
		  "category": "info",
		  "entries": [
			{
			  "id": 5,
			  "ival": [ 5230 ],
			  "nid": "PTrk.Msg.Tracking.Acquire"
			},
			{
			  "id": 6,
			  "ival": [ 3000 ],
			  "nid": "PTrk.Msg.Tracking.Innactivity"
			},
			{
			  "id": 3,
			  "nid": "PTrk.Msg.Tracking.Reporting"
			}
		  ],
		  "msgtype": "KeyValueMsg",
		  "nid": "PTrk.Msg.Tracking",
		  "sent": "2019-08-08T18:07:26.000000Z"
		},
		{
		  "epoch": "2019-08-08T18:07:26.000000Z",
		  "location": {
			"confidence": 0.6826890110969543,
			"floor": 0,
			"shape": {
			  "alt": -0.0003403102778705147,
			  "lat": 47.69714000560126,
			  "lon": -122.17326,
			  "radius": 459.1407470703125,
			  "type": 31,
			  "vert": 99.9999008178711
			},
			"veracity": 0.0
		  },
		  "msgtype": "LocationReport",
		  "sent": "2019-08-08T18:07:26.000000Z",
		  "updated": true
		}
	  ],
	  "resultcode": 1
	}

Review of the message contents shows that three messages were returned:  an Int32Msg, 
a KeyValueMsg, and Location Report.  The Int32Msg and KeyValueMsg describe changes
in the Emergency state and tracking configuration.   The term "nid" stands for Name Identifer, which
is a unique string value define in the FLEX OCM specifiation for the application deployed on the 
device.  In this example, the PedTrackerBasic application is deployed and has a OCM application
spec defined as shown here [PedTrackerBasic Spec](../flex/apps/PedTrackerBasic/PedTrackerBasic.ocm.json).

A quick review of the spec will show that there are two messages defined currently for the PedTrackerBasic
application with nids: *PTrk.Msg.Tracking*, and *PTrk.Msg.Emergency*. These were sent by the tag
in the decode response above. The location report provides information about the last known location
of the device,  in this case the field 'updated' is true, meaning that new location information was
provided in the packets decoded.   If no new location data is provided the 'updated' field will be false.

### Decode Message Structure Specification
The following diagram shows the complete structure of the DecodeInput and DecodeOuput data 
structures.  Whether transmitting in JSON or binary XMF, these structures define the schema
for the input and output.

![Decode class](images/translation_decode_class.png)

## Encoding Transmission Packets
Encoding is the process of converting the messages into an compact encoded format for
efficient transmission to the CP-Flex enabled tag device.  The general sequence for sending data to a tag is shown in the following diagram.

![Translation Decode](./images/translation_encode_seq.png)

The Tracking solution prepares one or more messages to transmit to the tag.   It sends these
messages to the Translation API Encode function, which encodes the data using the device's
associated Flex OCM specification.   The resulting encoded data is split into one or more packets for
transmission to the tag.  The download packets are returned to the Tracking Solution, which forwards
the data to the device using the appropriate LoRa network solution.

### Encode Example 
The following example shows demonstrates encoding messages to be sento to the LoRa Tag deployed with the
PedTrackerBasic application.   In this example, the tracking solution wants to send the following unencoded
message data.

	{
	  "devid": "flex_test1",
	  "messages": [
		{
		  "entries": [
			{
			  "ival": [ 4500 ],
			  "nid": "PTrk.Msg.Tracking.Acquire"
			},
			{
			  "ival": [ 600 ],
			  "nid": "PTrk.Msg.Tracking.Reporting"
			}
		  ],
		  "msgtype": "KeyValueMsg",
		  "nid": "PTrk.Msg.Tracking",
		  "sent": "2019-08-06T18:14:15.131529290Z"
		},
		{
		  "msgtype": "Int32Msg",
		  "nid": "PTrk.Msg.Emergency",
		  "priority": "high",
		  "sent": "2019-08-06T18:14:15.131529290Z",
		  "svals": [ "PTrk.Msg.Emergency.Query" ],
		  "vals": []
		}
	  ]
	}

These messages set the tracking configuration and queries the current state of the emergency functions.

The Translation service encodes the data and returns the following download payload.

	{
	  "devid": "flex_test1",
	  "download": [
		{
		  "dateTransport": "2019-08-06T18:14:15.133424316Z",
		  "gateways": [],
		  "payload": "AYF3w0kllgMcAxpAAgQAQQlBAQVDBJQRAABBCUEBA0MEWAIAAAIFCAICAAA="
		}
	  ],
	  "resultcode": 1
	}
The two messages are combined into a single transmission packet, which can be processed by
the flex application on the device.

### Encode Message Structure
The following diagram shows the complete structure of the EncodeInput and EncodeOutput data
structures.  Whether transmitting in JSON or binary XMF, these structures define the schema
for the input and output.

![Decode class](images/translation_encode_class.png)


## Complete Translation API Structure

The following diagram shows the compete structure for encoding and decoding messages using
the Translation API.

![translation API](./images/translation_api_class.png)


---
*Copyright 2019-2020, Codepoint Technologies, Inc.* 
*All Rights Reserved*
