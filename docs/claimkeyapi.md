# CP-Flex� Claims API 

The CP-Flex Claims Service provides  APIs for claiming and managing Codepoint manufactured/distributed device identities.   The claims system provides a 
secured, device independent registry of device identities that once claimed, can only be viewed by the organization claiming the device or its designees.  
The service can be used directly to pre-claim devices or fully integrated with the device registration APIs provided by the CP-Flex Provisioning Services.

If using devices in an untethered manner with no support from CP-Flex cloud stack, the Claims API provides third-party solutions a convenient method to use 
scanned Device URIs or claim keys contained in the device QR code.  Once claimed, the third-party solution can access the device identity data for onboarding with
various services.  To learn more about the claims API, please see the OpenAPI documentation:  http://global.cpflex.tech/console/api-docs/?urls.primaryName=Claims%20API

For examples working with the claims API see the [claims postman collection](../postman/CP-Flex&#32;Claims&#32;API.postman_collection.json)


See [Downlink Reference](DownlinkReference.md) for a description of the claims API.

---
*Copyright 2021-2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
