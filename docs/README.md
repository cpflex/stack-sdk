# CP-Flex� Service APIs Documentation
* **[How To Guides](./howto/readme.md)**
* **[Provisioning API](provisioningapi.md)**
* **[Integration API](DownlinkReference.md)**
* **[Translation API](translationapi.md)**
* **[Developer HUB API](hubapi.md)**

---
*Copyright 2019-2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
