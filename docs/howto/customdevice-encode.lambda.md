# Lambda Custom Device Encode Example

The following yaml shows a complete encoding lambda specification.

```yaml
- element:
    identifier: "lambda://integration/codec/encode:[device_apiuri]"
    label: Custom Device Encode Template
    locked: false
    visibility: protected
    description: >
      Lambda demonstrates implementation of a custom device message encoder 
      that can process third-party devices. 
  attributes:
    isAsync: "true"
  executionuri: //node/vm2:v1.0
  parameters:
    dtNow: "{Date}  Transaction date time reference."
    msg: "{LambdaCustomMsg} The custom message to encode."
  sourcepkg: |
      //****>> delete these documentation before adding lambda (takes up extra space that is unnecessary).
      /**
      * @typedef Content
      * @property {string} datatype The content payload datatype as defined by the lambda codec implementation.
      * @property {*} payload  The unencoded payload data as defined by the lambda codec implementation.
      */

      /**
      * Defines a LambdaCustomMsg message.
      * @typedef LambdaCustomMsg
      * @property {string} msgtype The message type value is "LambdaCustomMsg".
      * @property {Date}   sent  Date and time message is being sent or transported.
      * @property {Packet} packet  The raw packet data.
      * @property {Content} content  The decoded content.
      */

      /** 
      * Decode custom message
      * @param {Date} dtNow  Transaction date time reference.
      * @param {UplinkPayload} packet  The uplink payload packet to decode.
      * @returns {{messages: [LambdaCustomMsg]}} Returns object containing decode results.
      */
      //****<<

      /** 
      * Encodes a custom message
      * @param {Date} dtNow  Transaction date time reference.
      * @param {LambdaCustomMsg} msg  The custom message to encode.
      * @returns {{packets: [DownlinkPayload]}} Returns object containing encoded packets.
      */
      function encode( dtNow,  msg) {
          //TODO implement encoding logic.
          //Example:
          if( msg.content.datatype = "CompanyX.CustomDevice.Config" ) {
            return {packets: [{
              port: 10,
              payload: SGVsbG8gd29ybGQgdGhpcyBpcyBhbiBlbmNvZGVkIHN0cmluZw==

            }]};
          } else return {packets:[]};
      }

      //Dump input to console.
      console.info( JSON.stringify( args, null, 2));
      module.exports = encode( args.dtNow args.message);
```

The javascript formatted sourcepkg is reproduced below for clarity.

```javascript
  //****>> delete these documentation before adding lambda (takes up extra space that is unnecessary).
  /**
  * @typedef Content
  * @property {string} datatype The content payload datatype as defined by the lambda codec implementation.
  * @property {*} payload  The unencoded payload data as defined by the lambda codec implementation.
  */

  /**
  * Defines a LambdaCustomMsg message.
  * @typedef LambdaCustomMsg
  * @property {string} msgtype The message type value is "LambdaCustomMsg".
  * @property {Date}   sent  Date and time message is being sent or transported.
  * @property {Packet} packet  The raw packet data.
  * @property {Content} content  The decoded content.
  */

  /** 
  * Decode custom message
  * @param {Date} dtNow  Transaction date time reference.
  * @param {UplinkPayload} packet  The uplink payload packet to decode.
  * @returns {{messages: [LambdaCustomMsg]}} Returns object containing decode results.
  */
  //****<<

  /** 
  * Encodes a custom message
  * @param {Date} dtNow  Transaction date time reference.
  * @param {LambdaCustomMsg} msg  The custom message to encode.
  * @returns {{packets: [DownlinkPayload]}} Returns object containing encoded packets.
  */
  function encode( dtNow,  msg) {
      //TODO implement encoding logic.
      //Example:
      if( msg.content.datatype = "CompanyX.CustomDevice.Config" ) {
        return {packets: [{
          port: 10,
          payload: SGVsbG8gd29ybGQgdGhpcyBpcyBhbiBlbmNvZGVkIHN0cmluZw==

        }]};
      } else return {packets:[]};
  }

  //Dump input to console.
  console.info( JSON.stringify( args, null, 2));
  module.exports = encode( args.dtNow args.message);

```

---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*