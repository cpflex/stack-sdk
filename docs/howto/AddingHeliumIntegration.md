# Adding Helium Integration Support
CP-Flex Offers a service plugin to provide communications and provisioning integration with Helium.   
This document is a step-by-step guide to configuring an integration.

Before beginning your integration, be sure to install the CPACC command line tools.  These can be found
here  https://gitlab.com/cpflex/tools/cpacc.  These instructions assume you have a valid Cloud Stack APIKEY and can access the Helium Console API.

You can setup the integration to provide communication and or provisioning.  The sequence for setting up the integration is as follows:

## 1. Configure Helium Console
   * Open the Console. 
   * Set up a label for the devices that will route traffic to CP-Flex Cloud Stack.
     Copy the UUID of the label.  It will be in step 2.
   * Create an HTTP integration and name it so that it easy to associate it with the Cloud stack account.  For example,
     use the Cloud Stack path:  e.g., cstack-resellers-mycompany-customers-customer1
   * Specify the endpoint URL: http://global.cpflex.tech/integration/v1.0/helium/uplink
   * Add 'apikey' header with Cloud Stack APIKEY.  
   * Copy Downlink URL and Downlink Key to be used int he next step.
   * Save the HTTP integration and save a copy of the integration UUID for step 2 below..
   * Use the flows page to connect the label to the HTTP integration.   
   * Set up a device profile for the Cora CT1XXX and/or CSXXXX series devices.
      - CT1XXX tracker profile configuration: Disable ADR. Enable join Accept CF Lists.
      - CSXXXX sensor profile configuration: Enable ADR, Enable join Accept CF Lists.

## 2. Configure the Cloud Stack Helium Communication Integration
 * Create a blank yaml file to store the helium integration specification.  Name the file *helium.service.yaml*.
 * Specify the communication uplink/downlink integration.  The following yaml snippet
   can be used as a template. Copy this template into the blank YAML file and edit the fields with the brackets [].

   ``` yaml
   -  typeuri: //integration/provider/device/communication
      name: [service name: e.g. helium-customerx]
      apikey: [helium integration downlink key]
      attributes:
        fPort: "2"
        idIntegration: [helium integration UUID]
      description: "[optional description of the purpose of the integration]"
      resid: helium
      url: https://console-vip.helium.com/api/v1
      visibility: protected
   ```
   **NOTE**:  Set visibility to 'private' if sub-accounts are not allowed to access this integration. Integrations are often specified at the reseller account level to apply to multiple sub-accounts. In these cases, the visibility should be protected. This allows the Cloud Stack to access integration information when performing transactions in the context of a sub-account.

## 3. Configure the Cloud Stack Helium Provisioning Integration
   * Specify the provisioning integration.  Copy this template into the YAML file. 
     ``` yaml
     -  typeuri: //integration/provider/device/provisioning
        name: [service name: e.g. helium-customerx]
        apikey: [Helium console API Key]
        attributes:
          label_id: [Helium Label ID copied in step 1.]
        description: "[optional description of the purpose of the integration]"
        resid: helium
        url: https://console-vip.helium.com/api/v1
        visibility: protected
     ```
     Similar to the communication version, the visibility property affects how sub-accounts may access the integration information. The provisioning integration is optional.  If not specified, the helium device must be set up by other means outside the scope of Cloud Stack.   If specified, Cloud Stack will manage the lifecyle of the device within Helium Console.   No direct integration with helium console is required for the device other than troubleshooting.
   * Once the yaml file is specified, use the cpacc command line tool to add/update the service integration for the selected account.   For example,
     ``` bash
     cpacc account services add //resellers/mycompany/customers -s helium.service.yaml.
     ```

     This will install the service for the specified account,  at which time, uplinks to any configured devices should flow.  Validate the integration by adding devices to Cloud Stack and verify they were also added to the specified label in Helium Console.
  
## 4. Install the Service Integration into Cloud Stack
At this point the helium service is intalled.  To see a detailed list of installed services for a specified account use the following command to dump as YAML:
```bash
cpacc account services info //resellers/mycompany/customers -Y
```
A summary view can be shown using.
```bash
cpacc account services list //resellers/mycompany/customers -T
```
These commands are useful to review what service integrations are available for a specific sub-account.

Once setup, test the integration thoroughly to make sure all required functions are working as expected.

---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
