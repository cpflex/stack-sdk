# Integrating Communications with External Applications
CP-Flex Offers a service plugin to provide communications integration with any HTTP service supporting the [Generic Communication Message Consumer APIs](http://global.cpflex.tech/console/api-docs/?urls.primaryName=Integration%20API).  These APIs provide the means to receive (push) uplink messages and post downlink messages to devices.  When setting up an integration it is strongly recommended that solution providers implement support for the following specific HHTP API endpoints.

**Push APIs (Cloud Stack Outbound)**
* Receive decoded device uplink messages [/provider/generic/downlink_push](http://global.cpflex.tech/console/api-docs/?urls.primaryName=Integration%20API#/Generic%20Communication%20Network%20Provider/postProviderDownlinkPush)
* Receive Cloud Stack Events (Outbound) [/events/notification_push](http://global.cpflex.tech/console/api-docs/?urls.primaryName=Integration%20API#/CP-Flex%20Events/postEventNotificationPush)

**OTHER APIS (Cloud Stack Inbound)**
* Downlinking Messages [/consumer/generic/downlink](http://global.cpflex.tech/console/api-docs/?urls.primaryName=Integration%20API#/Generic%20Communication%20Message%20Consumer/postConsumerDownlink)

These APIs comprise the communication interface with Cloud Stack enabling applications to easily integrate and communicate with devices associated with one or more accounts.  

In order to use these APIs the service integrations must be configured in Cloud Stack to activate these endpoints for the specified accounts.  Application providers may define multiple service integrations as needed to route data to one or more external services supporting the API schema.   

This document is a step-by-step guide to configuring the communications and event integration.

Before beginning your integration, be sure to install the CPACC command line tools.  These can be found
here  https://gitlab.com/cpflex/tools/cpacc.  These instructions assume you have a valid Cloud Stack APIKEY and can access the Helium Console API.

You can setup the integration to provide communication and or provisioning.  The sequence for setting up the integration is as follows:


## 1. Define Generic Communication Consumer Integration
 * Create a blank yaml file to store the integration specification.  Name the file *my-app-endpoint.service.yaml*.
 * Specify the uplink/downlink integration information .  The following yaml snippet
   can be used as a template. Copy this template into the blank YAML file and edit the fields with the brackets [].

   ``` yaml
   -  typeuri: //integration/consumer/device/communication
      name:  [service name: e.g. my-app-endpoint]
      apikey: [optional API KEY for the application]
      attributes:
        apikeyname: [optional header API key field name 'apikey' is default]
      description: [optional description]
      label: [optional label]
      resid: generic_http_consumer
      url: "[required application endpoint URL: https://my-app-endpoint/cloudstack/uplinks]"
      visibility: protected
   ```
   **NOTE**:  Set visibility to 'private' if sub-accounts are not allowed to access this integration. Integrations are often specified at the reseller account level to apply to multiple sub-accounts. In these cases, the visibility should be protected. This allows the Cloud Stack to access integration information when performing transactions in the context of a sub-account.


## 2. Define Cloud Stack Event Integration
 * Add cloud stack event integration to the yaml file created in step 1 above.  
 * Copy / modify the YAML below to enable receipt of event notifications.
  ``` yaml
  - typeuri: //integration/events/notification/http
    name:  [service name: e.g. my-app-endpoint]
    apikey: [optional API KEY for the application]
    attributes:
      apikeyname: [optional header API key field name 'apikey' is default]
    description: [optional description]
    label: [optional label]
    resid: httpevents
    url: "[required application endpoint URL: https://my-app-endpoint/cloudstack/events]"
    visibility: protected
  ```

## 3. Install the service integrations to activate the integration.

Once integration definitions are completed, you can use the cpacc command line tool to install the integration and 
activate the information flow.  Alternatively, the [Cloud Stack Console](http://staging.cpflex.tech/console/app) can be used to verify integration services.

For example,

``` bash
 cpacc account services add //resellers/mycompany/customers -s my-app-endpoint.service.yaml.
```
  
## 3. Verify the Installation 
To see a detailed list of installed services for a specified account use the following command to dump as YAML:
```bash
cpacc account services info //resellers/mycompany/customers -Y
```
A summary view can be shown using.

```bash
cpacc account services list //resellers/mycompany/customers -T
```
These commands are useful to review what service integrations are available for a specific sub-account.

Once setup, test the integration thoroughly to make sure all required functions are working as expected.

---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
