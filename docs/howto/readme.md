# How To Guides for CP-Flex Cloud Stack

The following articles are available:
* [Getting Started with Cloud Stack](./GettingStartedWithCloudStack.md)
* [Integrating Communications with External Applications](./AddingGenericCommunicationConsumer.md)
* [Adding Helium integration Support](AddingHeliumIntegration.md)
* [Claiming Codepoint Supported Devices](ClaimingDevices.md)
* [Adding Custom Devices Using Lambdas](./AddingLambdaCustomDevice.md)

---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
