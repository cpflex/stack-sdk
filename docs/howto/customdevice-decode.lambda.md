# Lambda Custom Device Decode Example

The following yaml shows a complete decoding lambda specification.  

```yaml
- element:
    identifier: "lambda://integration/codec/decode:[device_apiuri]"
    label: Custom Device Codec Template
    locked: false
    visibility: protected
    description: >
      Lambda demonstrates implementation of a custom device message decoder 
      that can process third-party devices. 
  attributes:
    isAsync: "true"
  executionuri: //node/vm2:v1.0
  parameters:
    dtNow: "{Date} Transaction date time reference."
    packet: "{UplinkPacket} Specifies encoded or decoded message."
  sourcepkg: |
    //****>> delete these documentation before adding lambda (takes up extra space that is unnecessary).
    /**
    * Simple 3D spheroid location definition in rotational coordinates with optional floor and place information.
    * @typedef BasicLocation
    * @property {Number} lat Latitude of the gateway in degrees.
    * @property {Number} lon Longitude of the gateway in degrees.
    * @property {Number} alt Altitude of the gateway in degrees.
    * @property {Number} radius Horizontal uncertainty in meters.
    * @property {Number} vert vertical uncertainty in meters.
    * @property {Number} veracity Trustworthiness of the location 1.0 = 100% 0.0 = no trust (no veracity).
    * @property {Number} confidence Positioning confidence: range 0 to 1.0 Default is one standard deviation.
    * @property {Number} floorIndex Optional floor index.
    * @property {string} floorName Optional floor name.
    * @property {string} placeId Optional place identifier.
    * @property {string} placeName Optional place name.
    */

    /**
    * Gateway metadata.
    * @typedef Gateway
    * @property {string} gwid The gateway identifier.
    * @property {Date}  received The date-time message was received by the gateway.
    * @property {Number} channel The channel uplink was received.
    * @property {Number} rssi The uplink received signal strength indication in dBm.
    * @property {Number} snr Uplink received signal to noise ratio.
    * @property {BasicLocation} location Location of gateway.
    */

    /**
    * Information about the received uplink
    * @typedef UplinkMetadata
    * @property {Date} received The downlink port Number.
    * @property {Number} frequency The uplink frequency in MHz.
    * @property {string} modulation Modulation specifier. (.e.g LORA);
    * @property {string} lorawan_dr LoRaWAN data rate specifier DR0 - DR13.  The interpretation varies by region.
    * @property {string} data_rate Specifies the data rate comprising the spreading factor and Bandwidth (e.g. SF7BW125)
    * @property {Number} bit_rate bit rate in bits per second.
    * @property {string} coding_rate Specifies the symbol coding rate (e.g. 4/5).
    * @property {[Gateway]} gateways One or more uplink gateway data.
    */

    /**
    * @typedef UplinkPacket
    * @property {Date}   dateTransport Date and time uplink was transported.
    * @property {Number} port The downlink port Number.
    * @property {Number} counter Uplink message count.
    * @property {string} payload  Base64 encoded payload data.
    * @property {UplinkMetadata} metadata uplink metadata.
    * @property {BasicLocation} location  Network calculated location if available.
    */        

    /**
    * @typedef Content
    * @property {string} datatype The content payload datatype as defined by the lambda codec implementation.
    * @property {*} payload  The unencoded payload data as defined by the lambda codec implementation.
    */

    /**
    * Defines a LambdaCustomMsg message.
    * @typedef LambdaCustomMsg
    * @property {string} msgtype The message type value is "LambdaCustomMsg".
    * @property {Date}   sent  Date and time message is being sent or transported.
    * @property {Packet} packet  The raw packet data.
    * @property {Content} content  The decoded content.
    */
    //****<<

    /** 
    * Decode custom message
    * @param {Date} dtNow  Transaction date time reference.
    * @param {UplinkPacket} packet  The uplink payload packet to decode.
    * @returns {{messages: [LambdaCustomMsg]}} Returns object containing decode results.
    */
    function decode( dtNow, packet) {
        //TODO implement decoding logic.
        //Example:
        if( port ==12 ){
          return {messages:[{
            msgtype: "LambdaCustomMsg",
            sent: dtNow,            
            content: {
              datatype: "CompanyX.CustomDevice.Status",
              payload: {
                status: "ready",
                tickActivity: 46,
                temperature: 23                
              }
            }
          }]};            
        } else {
          return {messages:[{
            msgtype: "LambdaCustomMsg",
            sent: dtNow,            
            content: {
              datatype: "CompanyX.CustomDevice.NotSupported" 
            }
          }]};
        }
    }

    //Dump input to console.
    console.info( JSON.stringify( args, null, 2));
    module.exports = decode( args.dtNow, args.packet);
```
The javascript formatted sourcepkg is reproduced below for clarity.

```javascript
  //****>> delete these documentation before adding lambda (takes up extra space that is unnecessary).
  /**
  * Simple 3D spheroid location definition in rotational coordinates with optional floor and place information.
  * @typedef BasicLocation
  * @property {Number} lat Latitude of the gateway in degrees.
  * @property {Number} lon Longitude of the gateway in degrees.
  * @property {Number} alt Altitude of the gateway in degrees.
  * @property {Number} radius Horizontal uncertainty in meters.
  * @property {Number} vert vertical uncertainty in meters.
  * @property {Number} veracity Trustworthiness of the location 1.0 = 100% 0.0 = no trust (no veracity).
  * @property {Number} confidence Positioning confidence: range 0 to 1.0 Default is one standard deviation.
  * @property {Number} floorIndex Optional floor index.
  * @property {string} floorName Optional floor name.
  * @property {string} placeId Optional place identifier.
  * @property {string} placeName Optional place name.
  */

  /**
  * Gateway metadata.
  * @typedef Gateway
  * @property {string} gwid The gateway identifier.
  * @property {Date}  received The date-time message was received by the gateway.
  * @property {Number} channel The channel uplink was received.
  * @property {Number} rssi The uplink received signal strength indication in dBm.
  * @property {Number} snr Uplink received signal to noise ratio.
  * @property {BasicLocation} location Location of gateway.
  */

  /**
  * Information about the received uplink
  * @typedef UplinkMetadata
  * @property {Date} received The downlink port Number.
  * @property {Number} frequency The uplink frequency in MHz.
  * @property {string} modulation Modulation specifier. (.e.g LORA);
  * @property {string} lorawan_dr LoRaWAN data rate specifier DR0 - DR13.  The interpretation varies by region.
  * @property {string} data_rate Specifies the data rate comprising the spreading factor and Bandwidth (e.g. SF7BW125)
  * @property {Number} bit_rate bit rate in bits per second.
  * @property {string} coding_rate Specifies the symbol coding rate (e.g. 4/5).
  * @property {[Gateway]} gateways One or more uplink gateway data.
  */

  /**
  * @typedef UplinkPacket
  * @property {Date}   dateTransport Date and time uplink was transported.
  * @property {Number} port The downlink port Number.
  * @property {Number} counter Uplink message count.
  * @property {string} payload  Base64 encoded payload data.
  * @property {UplinkMetadata} metadata uplink metadata.
  * @property {BasicLocation} location  Network calculated location if available.
  */        

  /**
  * @typedef Content
  * @property {string} datatype The content payload datatype as defined by the lambda codec implementation.
  * @property {*} payload  The unencoded payload data as defined by the lambda codec implementation.
  */

  /**
  * Defines a LambdaCustomMsg message.
  * @typedef LambdaCustomMsg
  * @property {string} msgtype The message type value is "LambdaCustomMsg".
  * @property {Date}   sent  Date and time message is being sent or transported.
  * @property {Packet} packet  The raw packet data.
  * @property {Content} content  The decoded content.
  */
  //****<<

  /** 
  * Decode custom message
  * @param {Date} dtNow  Transaction date time reference.
  * @param {UplinkPacket} packet  The uplink payload packet to decode.
  * @returns {{messages: [LambdaCustomMsg]}} Returns object containing decode results.
  */
  function decode( packet) {
      //TODO implement decoding logic.
      //Example:
      if( port ==12 ){
        return {messages:[{
          packet: packet,
          content: {
            datatype: "CompanyX.CustomDevice.Status",
            payload: {
              status: "ready",
              tickActivity: 46,
              temperature: 23                
            }
          }
        }]};            
      } else {
        return {messages:[{
          packet:packet,
          content: {
            datatype: "CompanyX.CustomDevice.NotSupported" 
          }
        }]};
      }
  }

  //Dump input to console.
  console.info( JSON.stringify( args, null, 2));
  module.exports = decode( args.dtNow, args.packet);
```



---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*