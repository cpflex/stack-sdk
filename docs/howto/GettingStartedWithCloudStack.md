# Getting Started with CP-Flex Cloud Stack
CP-Flex Offers several APIs to configure various device and service configurations.   Before you begin, make sure you have received a valid API key providing 
access to the various APIs supported for your account.  Contact Codepoint support if you need help in obtaining an account and/or API key.

## 1. Cloud Stack Console 

Be sure you have created a login on the [Cloud Stack Console web application](http://staging.cpflex.tech/console/app).  This provides a workspace to work with Cloud Stack services during configuration and development.  Once created be sure to add an account reference. Given the provided API key add an account reference to your workspace to enable access to one or more cloud stack accounts.  If you do not have a an initial API key, please contact Codepoint.


## 2. Tools Setup
Currently, the best way to configure Cloud Stack services is through command line tools.  Please install one or more of these tools.

* [cpacc](https://gitlab.com/cpflex/tools/cpacc) - Command line tool for managing accounts, devices, subscriptions, and service integrations (e.g. LNS providers, and data consumers).
* [cpclaim](https://gitlab.com/cpflex/tools/cpclaim) - Command line tool for managing device claimkeys.   Useful for bulk services integration and claiming Cora 'untethered' sensors.
* [cphub](https://gitlab.com/cpflex/tools/cphub) - Command line tool for managing firmware and application packages.  This is for CT1XXX device application development only.
* [cpucc](https://gitlab.com/cpflex/tools/cpucc) - Comand line tool for encoding/decoding Codepoint Cora Untethered Protocol messages (CS1XXX series sensors).
  
For managing accounts, devices, and services, CPACC is the primary tool along for Cloud Stack.  Please make sure that is installed before proceeding.

## 3.  Defining Service Integrations
Cloud Stack manages integration, data processing, and service subscriptions for Codepoint and third-party devices supporting multiple LoRaWAN providers.  For resellers, Codepoint, will provide an initial services integration setup given solution requirements.  This enables device uplink data to flow to various applications and other services as well as providing downlink access as well.   Support is readily available for a variety of LNS providers.  Application conusmers can receive uplinks using the generic consumer APIs.   For more informaton on integrating services please see:

* [Integrating Communications with External Applications](./AddingGenericCommunicationConsumer.md)
* [Adding Helium Integration Support](./AddingHeliumIntegration.md) 
  
---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
