# Adding a Custom Device Codec to Cloud Stack

CP-Flex Cloud Stack supports third party device codec parsers using the Lambda scripting engine.   
Encoding and decoding scripts are implemented and uploaded such that they can be used by selected accounts.   

The codec is implemented as multiple lambda scripts supporting encode and decode operations.  The reason to separate the implementation is to reduce loading unnecessary code.  Typically, uplink decode will be called far more often that downlink encode and as such downlink encoding functions do not need to be stored in cache for repeated use.

Once a lambda script is complete.  Add the lambda to the chosen account using the one
of the following URIs.

* ```lambda://integration/device/codec/encode:[device_apiuri]```.  Use this URI path for 
encoding scripts.
* ```lambda://integration/device/codec/decode:[device_apiuri]```.  Use this URI path for
decoding scripts.

The [device_apiuri] value is a unique value within the scope of the chosen account that 
identifies the custom device protocol and version.   It is recommended that this value
be chosen with a basic structure [devicetype]_[version].  For devices that will used this
lambda to encode/decode data.  The device 'apiuri' field must have the same [device_apiuri] value.
This approach is flexible in that as versions changes, additional scripts can be provided
to handle new versions without affecting existing devices.

To encode/decode messages the system passes the script a LambdaCustomMsg object.  This object
can be modified providing the encoded/decoded data, where upon it is returned to the system
to continue processing.

The LambdaCustomMsg is documented in the sample lambda script below.   Here is one example converted to JSON showing encoded/decoded data might look like.


```json
{
  "msgtype": "LambdaCustomMsg",
  "sent": "2202-07-16T20:00:00.000Z",
  "packet": {
    "port": 18,
    "payload": "CAIQoAsaCHYwLjYuMC4w"
  },
  "content": {
    "datatype": "[brand].[devicetype].[messagetype]",
    "payload": {
      "status": "ready",
      "tickActivity": 46,
      "temperature": 23
    }
  }
}
```
## Define Custom Device Type 
To prepare the Cloud Stack for the new custom device, a new 'device-type' must be specified in the hub.   Future versions of the lambda system will support 
distribution of lambdas via the hub.  For now, though the device-type is required to indicate that it is supported.

Consider that we have a LoRaWAN device named 'example-v1' that we'd like to provide Lambda CODEC support in Cloud Stack.   The firs step is to define the 
device in the Hub using the following cphub command line.

```
cphub create device-type/lambda/[devicename (e.g. example)] package 
cphub update device-type/lambda/[devicename (e.g. example)]:v1  
```
This creates a new device entry ```device-type/lambda/[devicename]:v1```, which will be required when registering the devices.   If you plan to support claimkeys and single scan QR technology, please contact Codepoint support to register multiple devices in the claimkey registration database.  

For small deployments, the device/register API can be use directly to create the custom devices in Cloud Stack. Once the device type is specified, register the device
using the following procedure:

1. Define the device specification.  Manual registration requires creating a specification file with the applicable registration data.  See  [example-v1.device.yaml](../../examples/LambdaCustomCodecs/example-v1.device.yaml) for an example specification file.
   
2. Check the account that will be used to register the device for an existing registration key.  Depending upon the account having a subscription or not, registration may require a device registration subscription key.   Locate the key or request one from Codepoint support if needed.   
   
3. Register the device.
   If the account has an existing subscription (Account based subscription management), then the following command will register a custom Lambda device:

   ```
   cpacc device register [acct] -d [devEUI] -s example-v1.device.yaml
   ```

   For accounts required device registration keys the following should work.

   ```
   cpacc device register [acct] -d [devEUI] -s lambda-device.json -k [registration key]
   ```


## Lambda Custom Device Template

Below are markdown friendly examples of the lambdas that can be used as a template for defining third-party device codecs.

* [**Encoding Lambda Example**](./customdevice-encode.lambda.md)
* [**Decoding Lambda Example**](./customdevice-decode.lambda.md)

The following links are functional lambdas that can be installed

* [**encode-example-v1.lambda.yaml**](../../examples/LambdaCustomCodecs/encode-example-v1.lambda.yaml)
* [**decode-example-v1.lambda.yaml**](../../examples//LambdaCustomCodecs/decode-example-v1.lambda.yaml)

## Installing the Lambda Custom Codec
Installing lambdas requires access privileges to the Cloud-Stack Lambda APIs.  If you do not have an access key (acckey) for your root account,
please contact Codepoint support for obtaining access.    The following examples show how to install and update the lambda codec scripts
using the CPACC command line utility.

1. Set environment variables:
   ```
   set CPACC_APIKEY="[root account apikey]"
   set CPACC_ACCKEY="[root account acckey]"
   ```

2. Set your command line directory to where your lambdas are stored.  In this case:
   ```./examples/LambdaCustomCodecs```

2. Add the lambdas
   ```
   cpacc lambda add  //resellers/[rootaccount] "encode-example-v1.lambda.yaml"
   cpacc lambda add  //resellers/[rootaccount] "decode-example-v1.lambda.yaml"
   ```

## Updating the Lambda Custom Codecs.
As required the specified version other lambda can be updated or a different version installed. To update an existing version use the cpacc lambda update method.  Below are the 
commands showing how to update the lambdas from the previous section.

```
cpacc lambda update  //resellers/[rootaccount] "encode-example-v1.lambda.yaml"
cpacc lambda update  //resellers/[rootaccount] "decode-example-v1.lambda.yaml"

```

---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*