
# Claiming Codepoint Supported Devices

All Codepoint devices and partner supported devices are marked with a QR code containing a claim key or manufacturer independent device URI that can be scanned to retrieve device identity data.   To obtain the device’s identity information, the device must first be claimed by a registered reseller or customer (the Claimant).  Once claimed, the device information is only available to the Claimant or their authorized users. This prevents theft and renders the device useless if stolen for they cannot be repurposed without approval of the claimant.

The CP-Flex system provides a freely accessible claims API for registered users (those with an APIKEY).   Claiming and managing claimed devices can be done through the CP-Flex Cloud Stack Claims APIs or using one or more of the tools provided by Codepoint.  Currently, there are four methods available:

* [Codepoint Mobile Claims Application](#claiming-devices-using-mobile-claims-application) – Android and iOS supported application for smartphones.  Use this method to claim a few devices or to quickly retrieve information about claimed devices for manual data entry.
* [CPCLAIM](#claiming-devices-using-javascript-command-line-tools) – Javascript command line tools for Windows and Linux command line environments.   The tools is a lightweight wrapper on the claims API.  Making it easy to automate common administrative functions.  It is used typically for batch jobs or generating
  data files for bulk upload processes.
* [Claims Service API](#claiming-devices-using-the-claims-service-api) – Access the claim service directly to claim devices as part of an automated onboarding process.  
* [Provisioning Service Device Registration API](#claiming-devices-using-the-cp-flex-provisioning-apis) – For devices using the CP-Flex cloud stack provisioning services APIs, claiming devices is built into device registration APIs.  

Instructions for installing and using these tools are provided below.  Before you proceed with the following instructions for claiming devices, please be sure you have a reseller account with a valid APIKEY. Contact Codepoint if you need assistance.

## Claiming Devices Using Mobile Claims Application
Below is the information for downloading the Codepoint Claim Key Application mobile application, which is the quickest way to get the device information prior to integrating with your system.   

1.	Install Expo Go on a mobile device. You can find it in the device App Store.
2.	Scan the QR code below to load the Codepoint Claim tool instance for your  particular device OS.

    **iOS Version:**

    ![IOS QR Code](../images/qr-codepoint-claim-tool-ios.png)

    **Android Version:**

    ![android QR Code](../images/qr-codepoint-claim-tool-android.png)

3.	Once Loaded, Click the + button at the upper right and enter the reseller account APIKEY and press enter.  It is sometimes easier to read the APIKEY from an email on the mobile device and cut and paste the account API Key.
4.	Click the added account.
5.	Click the QR Scan button on the right side near the search box.   
6.	Scan the QR code on the back of the device,  claim the device.
7.	Repeat steps 5 and 6 for each device to be claimed.
8.	If successful, all devices will be claimed in the specified reseller account.  To view the identity information, tap the device.  Optionally, the device’s identity information can be copied and pasted for use elsewhere.  

## Claiming Devices Using Javascript Command Line Tools

CPCLAIM is a command line tool for claiming and manage Cora device identity data.  It provides the means to add, check, list, and purge claims for a specified account. 

1.	Clone the cpclaim tool onto your PC  
    ``` 
    git clone https://gitlab.com/cpflex/tools/cpclaim
    ```
2.	Follow the instructions for installing the tool in the README.md file.
3.	Set up the CPCLAIM_APIKEY environment variable with your APIKEY
4.	Use the ‘cpclaim registrar claim’ command to claim one or more devices in the account associated with the APIKEY.  The QR code found on the device contains the claim key information.
    ```
    cpclaim registrar claim .  [claimkeys …]
    ```
5.	Once claimed, it will return the device identity data. 
6.	To generate a CSV formatted file of all the claimed devices for the account associated with the APIKEY, use the following command.
    ```
    cpclaim registrar list . -C -F claimed-devices.csv
    ```
## Claiming Devices using the Claims Service API
See [Claim Service API](../claimkeyapi.md) for information on using the claim API.   

## Claiming Devices using the CP-Flex Provisioning APIs
Devices can be claimed while registering them using the provisioning APIs.  For devices that will be using the CP-Flex integration/translation functionality,
provisioning is required.  Using the [```/device/register```](http://global.cpflex.tech/console/api-docs/?urls.primaryName=Provisioning%20API#/Device/postDeviceRegister) 
or the [```/device/registertemplate```](http://global.cpflex.tech/console/api-docs/?urls.primaryName=Provisioning%20API#/Device/postDeviceRegisterTemplate) APIs,
unclaimed devices will be automatically claimed to the specified account.  If a device is already claimed by a different account that is not an ancestor, registration will fail.   
The claimkey or device URI can be used to register the device.


---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
