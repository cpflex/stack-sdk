////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: Kiata_Translation_SpeedInfo.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Structure `SpeedInfo`

┗{nbsp}Kiata.Translation.SpeedInfo

*namespace*: Kiata.Translation +
*packed*: true 

.Description
Structure contains speed information of the object. +


=== Summary

.Fields defined in Kiata.Translation.SpeedInfo
<<Kiata_Translation_SpeedInfo.adoc#F1,horizontal>>, <<Kiata_Translation_SpeedInfo.adoc#F2,vertical>>, <<Kiata_Translation_SpeedInfo.adoc#F3,azimuth>>, <<Kiata_Translation_SpeedInfo.adoc#F4,uncertainty>>

=== Field Detail
[[F1]]
horizontal (required) : <<Tensor_float32.adoc#,float32>>, in meters/sec ::
Optional horizontal speed of device in meters/sec +

[[F2]]
vertical (required) : <<Tensor_float32.adoc#,float32>>, in meters/sec ::
Optional vertical speed of device in meters/sec +

[[F3]]
azimuth (required) : <<Tensor_float32.adoc#,float32>>, in degrees ::
Optional azimuthal heading of the associated object in degrees (true North). +

[[F4]]
uncertainty (required) : <<Tensor_float32.adoc#,float32>>, in meters/sec ::
Optional speed uncertainty in meters/sec. +




=== See Also
.Structures
<<Kiata_Translation_FlexMessage.adoc#,FlexMessage>>, <<Kiata_Translation_KeyValueMsg.adoc#,KeyValueMsg>>, <<Kiata_Translation_LoRaGatewayInfo.adoc#,LoRaGatewayInfo>>, <<Kiata_Translation_Message.adoc#,Message>>, <<Kiata_Translation_UnencodedMsg.adoc#,UnencodedMsg>>
<<Kiata_Translation_Location.adoc#,Location>>, <<Kiata_Translation_TransportPayload.adoc#,TransportPayload>>, <<Kiata_Translation_InvalidMsg.adoc#,InvalidMsg>>, <<Kiata_Translation_Int32Msg.adoc#,Int32Msg>>, <<Kiata_Translation_LocationReport.adoc#,LocationReport>>

.Interfaces
<<Kiata_Translation_Translator.adoc#,Translator>>

