////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: GeoSpatial_Shape.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Structure `Shape`

┗{nbsp}GeoSpatial.Shape

*namespace*: GeoSpatial +
*RTT Field*: <<GeoSpatial_Shape.adoc#F1,type>> +
*packed*: false 

.Description
Shapes are used to describe various kinds of geometry. +
When used in an Estimate result the Shape represents the
                uncertainty area (2D) or uncertainty volume (3D) at the
                confidence in the Estimate. +


=== Summary

.Fields defined in GeoSpatial.Shape
<<GeoSpatial_Shape.adoc#F1,type>>

=== Field Detail
[[F1]]
type (required) : <<GeoSpatial_ShapeType.adoc#,ShapeType>> ::
This is the one thing all shapes have in common +




=== See Also
.Structures
<<GeoSpatial_BoundingBox.adoc#,BoundingBox>>, <<GeoSpatial_Circle.adoc#,Circle>>, <<GeoSpatial_Point2D.adoc#,Point2D>>, <<GeoSpatial_AddressInfo.adoc#,AddressInfo>>, <<GeoSpatial_Point3D.adoc#,Point3D>>
<<GeoSpatial_PositionECEF.adoc#,PositionECEF>>, <<GeoSpatial_Ellipse.adoc#,Ellipse>>, <<GeoSpatial_GeoObject.adoc#,GeoObject>>, <<GeoSpatial_Spheroid.adoc#,Spheroid>>, <<GeoSpatial_Location.adoc#,Location>>
<<GeoSpatial_Object.adoc#,Object>>, <<GeoSpatial_Ellipsoid.adoc#,Ellipsoid>>

.Enumerations
<<GeoSpatial_ShapeType.adoc#,ShapeType>>, <<GeoSpatial_ObjectType.adoc#,ObjectType>>, <<GeoSpatial_ReferenceFrame.adoc#,ReferenceFrame>>

.Other Types
<<GeoSpatial_XYZVector.adoc#,XYZVector>>, <<GeoSpatial_ShapeTypeArray.adoc#,ShapeTypeArray>>

