////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: GeoSpatial_ShapeType.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Enumeration `ShapeType`

*namespace*: GeoSpatial +
*type*: <<Tensor_int8.adoc#,Tensor.int8>> +
*default*: GeoSpatial.ShapeType.undefined

.Description
{nbsp}

=== Literals
[[L1]]
undefined : 0 ::
{nbsp}
[[L2]]
Covariance : 1 ::
{nbsp}
[[L3]]
Point2D : 20 ::
{nbsp}
[[L4]]
Circle : 21 ::
{nbsp}
[[L5]]
Ellipse : 22 ::
{nbsp}
[[L6]]
Arc : 23 ::
{nbsp}
[[L7]]
ArcBand : 24 ::
{nbsp}
[[L8]]
Polygon : 25 ::
{nbsp}
[[L9]]
Point3D : 30 ::
{nbsp}
[[L10]]
Spheroid : 31 ::
{nbsp}
[[L11]]
Ellipsoid : 32 ::
{nbsp}
[[L12]]
Arc3D : 33 ::
{nbsp}
[[L13]]
ArcBand3D : 34 ::
{nbsp}
[[L14]]
Polygon3D : 35 ::
{nbsp}



=== See Also
.Structures
<<GeoSpatial_BoundingBox.adoc#,BoundingBox>>, <<GeoSpatial_Circle.adoc#,Circle>>, <<GeoSpatial_Point2D.adoc#,Point2D>>, <<GeoSpatial_AddressInfo.adoc#,AddressInfo>>, <<GeoSpatial_Point3D.adoc#,Point3D>>
<<GeoSpatial_PositionECEF.adoc#,PositionECEF>>, <<GeoSpatial_Ellipse.adoc#,Ellipse>>, <<GeoSpatial_Shape.adoc#,Shape>>, <<GeoSpatial_GeoObject.adoc#,GeoObject>>, <<GeoSpatial_Spheroid.adoc#,Spheroid>>
<<GeoSpatial_Location.adoc#,Location>>, <<GeoSpatial_Object.adoc#,Object>>, <<GeoSpatial_Ellipsoid.adoc#,Ellipsoid>>

.Enumerations
<<GeoSpatial_ObjectType.adoc#,ObjectType>>, <<GeoSpatial_ReferenceFrame.adoc#,ReferenceFrame>>

.Other Types
<<GeoSpatial_XYZVector.adoc#,XYZVector>>, <<GeoSpatial_ShapeTypeArray.adoc#,ShapeTypeArray>>

