////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: Kiata_Translation_Translator_Encode_Output.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Structure `Output`

<<Tensor_OutputBase.adoc#,Tensor.OutputBase>> +
{nbsp}{nbsp}{nbsp}┗{nbsp}Kiata.Translation.Translator.Encode.Output

*namespace*: Kiata.Translation.Translator.Encode +
*sid*: 4099 (Interaction ID) +
*packed*: false 

.Description
{nbsp}

=== Summary

.Inherited fields defined in <<Tensor_OutputBase.adoc#,Tensor.OutputBase>>
<<F1,devid>>, <<F2,etid>>, <<F3,resultcode>>, <<F4,errinfo>>

.Fields defined in Kiata.Translation.Translator.Encode.Output
<<Kiata_Translation_Translator_Encode_Output.adoc#F1,download>>

=== Field Detail
[[F1]]
download [one or more] : <<Kiata_Translation_TransportPayload.adoc#,TransportPayload>> ::
Prepared download payloads. +




=== See Also
.Structures
<<Kiata_Translation_Translator_Encode_Input.adoc#,Input>>

