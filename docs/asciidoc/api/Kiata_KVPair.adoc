////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: Kiata_KVPair.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Structure `KVPair`

┗{nbsp}Kiata.KVPair

*namespace*: Kiata +
*packed*: false 

.Description
Specifies a simple key value pair comprising a uint8 identifier and a string. Either nid or id must be specified. +


=== Summary

.Fields defined in Kiata.KVPair
<<Kiata_KVPair.adoc#F1,nid>>, <<Kiata_KVPair.adoc#F2,id>>, <<Kiata_KVPair.adoc#F3,sval>>, <<Kiata_KVPair.adoc#F4,ival>>, <<Kiata_KVPair.adoc#F5,raw>>

=== Field Detail
[[F1]]
nid (optional) : <<Tensor_string.adoc#,string>> ::
Optional String identifier, typically used on cloud API interfaces, where transport size is not an issue. +

[[F2]]
id (optional) : <<Tensor_uint8.adoc#,uint8>> ::
Unique identifier for the pair, must be specified if nid is not present. +

[[F3]]
sval (optional) : <<Tensor_string.adoc#,string>> ::
Optional String value +

[[F4]]
ival (optional) : <<Kiata_Int32Array.adoc#,Int32Array>> ::
Optional Unsigned integer values +

[[F5]]
raw (optional) : <<Tensor_raw.adoc#,raw>> ::
Optional raw binary data. +




=== See Also
.Enumerations
<<Kiata_MessageFlags.adoc#,MessageFlags>>

.Other Types
<<Kiata_Int32Array.adoc#,Int32Array>>

