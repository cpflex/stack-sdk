////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: Tensor_NameValue.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Structure `NameValue`

┗{nbsp}Tensor.NameValue

*namespace*: Tensor +
*packed*: false 

.Description
A string base name value pair entry. +


=== Summary

.Fields defined in Tensor.NameValue
<<Tensor_NameValue.adoc#F1,name>>, <<Tensor_NameValue.adoc#F2,value>>

=== Field Detail
[[F1]]
name (required) : <<Tensor_string.adoc#,string>> ::
{nbsp}
[[F2]]
value (required) : <<Tensor_string.adoc#,string>> ::
{nbsp}



=== See Also
.Structures
<<Tensor_VoidType.adoc#,VoidType>>, <<Tensor_OutputBase.adoc#,OutputBase>>, <<Tensor_InputBase.adoc#,InputBase>>

.Enumerations
<<Tensor_WirelineFormat.adoc#,WirelineFormat>>, <<Tensor_ResultCode.adoc#,ResultCode>>, <<Tensor_ReservedEncodingFormats.adoc#,ReservedEncodingFormats>>, <<Tensor_InteractionRole.adoc#,InteractionRole>>, <<Tensor_ContentType.adoc#,ContentType>>

.Other Types
<<Tensor_uint32.adoc#,uint32>>, <<Tensor_xmf.adoc#,xmf>>, <<Tensor_raw.adoc#,raw>>, <<Tensor_string.adoc#,string>>, <<Tensor_uint16.adoc#,uint16>>
<<Tensor_uint24.adoc#,uint24>>, <<Tensor_UUID.adoc#,UUID>>, <<Tensor_int32.adoc#,int32>>, <<Tensor_int24.adoc#,int24>>, <<Tensor_int8.adoc#,int8>>
<<Tensor_uint64.adoc#,uint64>>, <<Tensor_int16.adoc#,int16>>, <<Tensor_EncodingFormat.adoc#,EncodingFormat>>, <<Tensor_mac48.adoc#,mac48>>, <<Tensor_float64.adoc#,float64>>
<<Tensor_void.adoc#,void>>, <<Tensor_int64.adoc#,int64>>, <<Tensor_uint8.adoc#,uint8>>, <<Tensor_uri.adoc#,uri>>, <<Tensor_json.adoc#,json>>
<<Tensor_bool.adoc#,bool>>, <<Tensor_float32.adoc#,float32>>, <<Tensor_byte.adoc#,byte>>, <<Tensor_datetime.adoc#,datetime>>

