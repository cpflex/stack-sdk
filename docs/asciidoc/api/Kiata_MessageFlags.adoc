////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: Kiata_MessageFlags.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Enumeration `MessageFlags`

*namespace*: Kiata +
*type*: <<Tensor_uint8.adoc#,Tensor.uint8>> +
*default*: Kiata.MessageFlags.Unspecified

.Description
Defines the priority of action messages. +


=== Literals
[[L1]]
Unspecified : 0 ::
{nbsp}
[[L2]]
Critical : 1 ::
{nbsp}
[[L3]]
High : 2 ::
{nbsp}
[[L4]]
Med : 3 ::
{nbsp}
[[L5]]
Low : 4 ::
{nbsp}
[[L6]]
Error : 16 ::
{nbsp}
[[L7]]
Warning : 32 ::
{nbsp}
[[L8]]
Alert : 48 ::
{nbsp}
[[L9]]
Info : 64 ::
{nbsp}
[[L10]]
Debug : 80 ::
{nbsp}
[[L11]]
TypeMask : 240 ::
{nbsp}
[[L12]]
PriorityMask : 15 ::
{nbsp}



=== See Also
.Structures
<<Kiata_KVPair.adoc#,KVPair>>

.Other Types
<<Kiata_Int32Array.adoc#,Int32Array>>

