////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: GeoSpatial_XYZVector.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Array `XYZVector`

*namespace*: GeoSpatial +
*type*: <<Tensor_float64.adoc#,Tensor.float64>> [3]

.Description
Defines 3D Cartesian Vector data array. +



=== See Also
.Structures
<<GeoSpatial_BoundingBox.adoc#,BoundingBox>>, <<GeoSpatial_Circle.adoc#,Circle>>, <<GeoSpatial_Point2D.adoc#,Point2D>>, <<GeoSpatial_AddressInfo.adoc#,AddressInfo>>, <<GeoSpatial_Point3D.adoc#,Point3D>>
<<GeoSpatial_PositionECEF.adoc#,PositionECEF>>, <<GeoSpatial_Ellipse.adoc#,Ellipse>>, <<GeoSpatial_Shape.adoc#,Shape>>, <<GeoSpatial_GeoObject.adoc#,GeoObject>>, <<GeoSpatial_Spheroid.adoc#,Spheroid>>
<<GeoSpatial_Location.adoc#,Location>>, <<GeoSpatial_Object.adoc#,Object>>, <<GeoSpatial_Ellipsoid.adoc#,Ellipsoid>>

.Enumerations
<<GeoSpatial_ShapeType.adoc#,ShapeType>>, <<GeoSpatial_ObjectType.adoc#,ObjectType>>, <<GeoSpatial_ReferenceFrame.adoc#,ReferenceFrame>>

.Other Types
<<GeoSpatial_ShapeTypeArray.adoc#,ShapeTypeArray>>

