////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: Kiata_Translation_Translator_Encode_Input.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Structure `Input`

<<Tensor_InputBase.adoc#,Tensor.InputBase>> +
{nbsp}{nbsp}{nbsp}┗{nbsp}Kiata.Translation.Translator.Encode.Input

*namespace*: Kiata.Translation.Translator.Encode +
*sid*: 4098 (Interaction ID) +
*packed*: false 

.Description
{nbsp}

=== Summary

.Inherited fields defined in <<Tensor_InputBase.adoc#,Tensor.InputBase>>
<<F1,devid>>, <<F2,etid>>, <<F3,directive>>

.Fields defined in Kiata.Translation.Translator.Encode.Input
<<Kiata_Translation_Translator_Encode_Input.adoc#F1,messages>>

=== Field Detail
[[F1]]
messages [one or more] : <<Kiata_Translation_Message.adoc#,Message>> ::
The messages to send to the device. +




=== See Also
.Structures
<<Kiata_Translation_Translator_Encode_Output.adoc#,Output>>

