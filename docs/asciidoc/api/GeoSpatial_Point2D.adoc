////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: GeoSpatial_Point2D.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Structure `Point2D`

<<GeoSpatial_Shape.adoc#,GeoSpatial.Shape>> +
{nbsp}{nbsp}{nbsp}┗{nbsp}GeoSpatial.Point2D

*namespace*: GeoSpatial +
*<<F1,type>> RTT key*: GeoSpatial.ShapeType.Point2D +
*packed*: false 

.Description
Defines a 2D (lat, lon) point in WGS84 coordinates. +


=== Summary

.Inherited fields defined in <<GeoSpatial_Shape.adoc#,GeoSpatial.Shape>>
<<F1,type>>

.Fields defined in GeoSpatial.Point2D
<<GeoSpatial_Point2D.adoc#F1,lat>>, <<GeoSpatial_Point2D.adoc#F2,lon>>

=== Field Detail
[[F1]]
lat (required) : <<Tensor_float64.adoc#,float64>>, in degrees ::
{nbsp}
[[F2]]
lon (required) : <<Tensor_float64.adoc#,float64>>, in degrees ::
{nbsp}



=== See Also
.Structures
<<GeoSpatial_BoundingBox.adoc#,BoundingBox>>, <<GeoSpatial_Circle.adoc#,Circle>>, <<GeoSpatial_AddressInfo.adoc#,AddressInfo>>, <<GeoSpatial_Point3D.adoc#,Point3D>>, <<GeoSpatial_PositionECEF.adoc#,PositionECEF>>
<<GeoSpatial_Ellipse.adoc#,Ellipse>>, <<GeoSpatial_Shape.adoc#,Shape>>, <<GeoSpatial_GeoObject.adoc#,GeoObject>>, <<GeoSpatial_Spheroid.adoc#,Spheroid>>, <<GeoSpatial_Location.adoc#,Location>>
<<GeoSpatial_Object.adoc#,Object>>, <<GeoSpatial_Ellipsoid.adoc#,Ellipsoid>>

.Enumerations
<<GeoSpatial_ShapeType.adoc#,ShapeType>>, <<GeoSpatial_ObjectType.adoc#,ObjectType>>, <<GeoSpatial_ReferenceFrame.adoc#,ReferenceFrame>>

.Other Types
<<GeoSpatial_XYZVector.adoc#,XYZVector>>, <<GeoSpatial_ShapeTypeArray.adoc#,ShapeTypeArray>>

