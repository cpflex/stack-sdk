////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: GeoSpatial_Circle.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Structure `Circle`

<<GeoSpatial_Shape.adoc#,GeoSpatial.Shape>> +
{nbsp}{nbsp}{nbsp}┗{nbsp}<<GeoSpatial_Point2D.adoc#,GeoSpatial.Point2D>> +
{nbsp}{nbsp}{nbsp}{nbsp}{nbsp}{nbsp}┗{nbsp}GeoSpatial.Circle

*namespace*: GeoSpatial +
*<<F1,type>> RTT key*: GeoSpatial.ShapeType.Circle +
*packed*: false 

.Description
In an Estimate, the shape is the uncertainty area. +


=== Summary

.Inherited fields defined in <<GeoSpatial_Shape.adoc#,GeoSpatial.Shape>>
<<F1,type>>

.Inherited fields defined in <<GeoSpatial_Point2D.adoc#,GeoSpatial.Point2D>>
<<F1,lat>>, <<F2,lon>>

.Fields defined in GeoSpatial.Circle
<<GeoSpatial_Circle.adoc#F1,radius>>

=== Field Detail
[[F1]]
radius (required) : <<Tensor_float32.adoc#,float32>>, in meters ::
In an estimate, radius is the horizontal uncertainty. +
0 <= radius +




=== See Also
.Structures
<<GeoSpatial_BoundingBox.adoc#,BoundingBox>>, <<GeoSpatial_Point2D.adoc#,Point2D>>, <<GeoSpatial_AddressInfo.adoc#,AddressInfo>>, <<GeoSpatial_Point3D.adoc#,Point3D>>, <<GeoSpatial_PositionECEF.adoc#,PositionECEF>>
<<GeoSpatial_Ellipse.adoc#,Ellipse>>, <<GeoSpatial_Shape.adoc#,Shape>>, <<GeoSpatial_GeoObject.adoc#,GeoObject>>, <<GeoSpatial_Spheroid.adoc#,Spheroid>>, <<GeoSpatial_Location.adoc#,Location>>
<<GeoSpatial_Object.adoc#,Object>>, <<GeoSpatial_Ellipsoid.adoc#,Ellipsoid>>

.Enumerations
<<GeoSpatial_ShapeType.adoc#,ShapeType>>, <<GeoSpatial_ObjectType.adoc#,ObjectType>>, <<GeoSpatial_ReferenceFrame.adoc#,ReferenceFrame>>

.Other Types
<<GeoSpatial_XYZVector.adoc#,XYZVector>>, <<GeoSpatial_ShapeTypeArray.adoc#,ShapeTypeArray>>

