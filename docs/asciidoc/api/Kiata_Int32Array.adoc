////
Copyright (c) 2019, Codepoint Technologies, Inc.,

All rights reserved.
Codepoint Technologies, Inc. PROPRIETARY/CONFIDENTIAL.
Use is subject to license terms included in the distribution.
////
:filename: Kiata_Int32Array.adoc
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]

_<<index_toc.adoc#,Contents>>, <<index_hierarchy.adoc#,Hierarchy>>_

== Array `Int32Array`

*namespace*: Kiata +
*type*: <<Tensor_int32.adoc#,Tensor.int32>> [*]

.Description
{nbsp}


=== See Also
.Structures
<<Kiata_KVPair.adoc#,KVPair>>

.Enumerations
<<Kiata_MessageFlags.adoc#,MessageFlags>>

