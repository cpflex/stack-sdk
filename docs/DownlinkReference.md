
## Downlink Message Using Generic Consumer 

To send messages to the device powered by the CP-Flex platform, you post one or more messages to the downlink
url available in the CP-Flex Integration service (http://global.cpflex.tech/integration/v1.0/generic/downlink.  
This documentation is preliminary.  Work is ongoing to produce the OpenApi specification.  

For now, postman collections and associated environments are available in the integration API collection.

The remainder of this document describes the API and formatting. The body of the post contains the JSON with 
the following post response structure.

You can sumbmit three types of messages:

* KeyValueMsg -- CP-Flex Application defined key value pairs.
* Int32Msg    -- CP-Flex Application defined Int32 data values.
* UnencodedMsg -- Unencoded messages that are not processed by the CP-Flex Codec.

The CP-Flex application messages are constrained to the messages defined by the tags installed CP-Flex application and assosicate OCM interface specification.   For example, look at the ocm.json specification in the tag_demo 
applications available in the Tag SDK.  

### JSON Post Structure

The following is the specification of all json fields and objects that can be used
when posting.   One or messages can be sent simultaneously.  However, be careful not to overload.  
LP-WAN technologies should use downlinks sparingly.

The post interface requires the caller to provide the provider name.  Currently, only "linkware" is the supported provider.  Updates shall be provided once the protocols are implemented for other providers.

```
{
    "devid" : "<required device identifier>",
    "etid"  : "<optional external transaction identifier>",
    "confirm"  : <Optional, true requires receive confirmation, false no confirmation needed.>,
    "schedule" : "<Optional, Last - place last in queue (default), First - place first in queue>",
    "ttl" : <Required: time to live, 2h>,
    "messages" : [
        {
            "msgtype" : "KeyValueMsg",
            "priority" : "<optional priority specifier: unspecified (default), critical, high, med, low>",
            "category" : "<optional category specifier: unspecified (default), error, warning, alert, info, detail, debug>",
            "nid" : "<required: message named identifier per OCM spec>",
            "sent" : "<required: timetag date and time message is sent>"
            "entries" : [
                {
                    "nid" : "<required key named id per OCM spec>",
                    "sval" : [ "<optional nid string array value per OCM spec>" ],
                    "ival" : [ <optional integer array value per OCM spec> ],
                    "raw"  : [ "<optional raw binary array data encoded in base  64>" ]
                }
            ]
        },
        {
            "msgtype" : "Int32Msg"
            "priority" : "<optional priority specifier: unspecified (default), critical, high, med, low>",
            "category" : "<optional category specifier: unspecified (default), error, warning, alert, info, detail, debug>",
            "nid" : "<required: message named identifier per OCM spec>",
            "sent" : "<required: timetag date and time message is sent>"
            "svals" : ["<optional nid string value array  per OCM spec>"],
            "vals" : [<optional integer value array per OCM spec>]
        },
        {
            "msgtype" : "UnencodedMsg"
            "priority" : "<optional priority specifier: unspecified (default), critical, high, med, low>",
            "category" : "<optional category specifier: unspecified (default), error, warning, alert, info, detail, debug>",
            "nid" : "<required: message named identifier per OCM spec>",
            "rawdata" :  [ "<optional array of base64 encoded binary data>" ],
            "jsondata" : [ "<optional array of json (string encoded) data>" ],
            "textdata" : [ "<optional array of strings>" ]
        }
    ]
}
```
### Response:

The downlink post will respond with the following json structure indicating the
result of processing request.
  
```
{
    "devid" : "<device identifier",
    "resultcode" : <result code>,
    "etid" : <external transaction>,
    "errinfo": "<information about the error>"
}
```

The returned result codes will be one of the following values.

```
  success          =  1,
  undefined        =  0,
  failure          = -1,
  busy             = -2,
  processingError  = -3,
  parsingError     = -4,
  malformedRequest = -5,
  encodingError    = -6
```


## HTTP Post
The following is an example post for disabling emergency mode for devices with the Ped. Tracker application installed.

**POST URL**
```
http://global.cpflex.tech/integration/v1.0/generic/downlink
```

**JSON Body**
```
{
  "devid": "[CP-Flex Devid]",
  "etid": "locator-downlink-test",
  "provider": "linkware",
  "confirm": true,
  "ttl": "2h",
  "schedule": "Last",
  "messages": [
    {
      "msgtype": "Int32Msg",
      "nid": "Ped.Cmds.Emergency",
      "svals": "Disable",
      "sent": "2020-12-17T06:51:05.038Z"
    }
  ]
}



---
*Copyright 2019-2020, Codepoint Technologies,* 
*All Rights Reserved*
  
