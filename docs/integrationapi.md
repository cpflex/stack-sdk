# CP-Flex� Integration API 

The CP-Flex integration service provides a data-defined integration of network providers and service consumers. The goal is
to significantly reduce system integration complexity for those deployments where carrier and application providers are supported.  
The service is extensible through plugins adding additional providers and consumer types as required.   It is recommended
that supported integrations be reviewed prior to developing your own integration plan.   If an integration is supported deployment
can be accomplished by simply configuring integration services for you selected account hierarchy.  

## Supported LNS Providers
CP-Flex has support for the following 3rd-party LNS providers.  Except where noted, these available integrations support both uplink/downlink communications and device provisioning/onboarding.  Integration 
is controlled through appropriate configuration of account services.

* Chirpstack
* Easylinkin Linkware  (communications only)
* Helium
* Loriot
* Senet
* TTE / TTN (V2.0)
* Tencent (partial support)

For partners wanting to implement their own integration support,  CP-Flex has the Generic HTTP Provider APIs.  

## Supported Service Consumers
* Generic HTTP Consumer (Downlink Only)
* Traxmate Consumer (Communications, Device Management)
* Custom Consumer (Contact CP-Flex Support)

See [Downlink Reference](DownlinkReference.md) for a preliminary description of the intration API.

---
*Copyright 2019-2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
