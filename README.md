﻿# CP-Flex™ Cloud Stack API SDK 


This package contains the documentation and examples for working with the CP-Flex Cloud Stack Services API.  To get started with Cloud Stack see [Getting Started](./docs/howto/GettingStartedWithCloudStack.md).

## Overview

![CP-Flex services](./docs/images/SystemOverview.png)

The CP-Flex Cloud Stack APIs provide access to the core CP-Flex functions needed to manage and interact with devices across
multiple network providers.   The CP-Flex powered devices support a light-weight application environment,
which enhances flexibility, maximum performance, and long battery life. The initial 
service provides message translation, indoor/outdoor location, provisioning, and application registration. 

They are described in brief as follows, click on the headers to link to specific API development documentation:

- [**Application Integration API**](./docs/integrationapi.md) – Integration services map various network providers and service consumers
  simplifying integration.  
 
- [**Tag Provisioning API**](./docs/provisioningapi.md) – API simplifies the process of provisioning and managing large-numbers of tags by 
aggregating the tag provisioning process simplifying the complicated activation and update activities 
involving multiple services.  Solution providers can use the provided CP-Flex™ provisioning tools
for Android/iOS or integration the functionality into their own mobile applications.   

- [**Message Translation API**](./docs/translationapi.md) – Messages are encoded to minimize transport data requirements. 
The Message Translation API enables Customer Tracking Solutions to readily encode and decode messages 
providing a simple and intuitive JSON based web API for web application developments.

- [**Developer Hub API**](./docs/hubapi.md) – The developer hub provides development and CP-Flex application 
  publication services enabling radip distribution of applications to users and devices in the ecosystem. 

The platform implements location determination capabilities at its 
core, such that no matter the message content the most-recent known location and time is always 
reported.  Location data like time tags are essential metadata and the service infrastructure provides 
the best estimate of location given measurement data collected by the tag both indoors and outdoors.  
Indoor location can be further enhanced through probe and survey mechanisms depending on customer 
requirements. Location functions are built into the CP-Flex service, there is not direct interaction required.

## SDK Contents
The SDK is currently light on documentation other than what is described above.  The primary means
for communicating what is required is by example.   The SDK provides an example folder comprising Postman collections and
open API specifications of the APIs.

More information can be found here:

* [How To Guides for CP-Flex Cloud Stack](./docs/howto/readme.md)
* [Open API Specifications](./openapi/README.md)
* [API Documentation](./docs/README.md)
* [Postman Example Collection](./postman/README.md)

---
*Copyright 2020-2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
