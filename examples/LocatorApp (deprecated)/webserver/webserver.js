'use strict';
// 45678901234567890123456789012345678901234567890123456789012345678901234567890
//
// NAME: linkwareadaptor.js
//
// =============================================================================

var chalk = require('chalk');
chalk.enabled = true;

const url = require('url');
const fs = require('fs');

var util = require('util');
var path = require('path');
var request = require('request');
var express = require('express');

var http = require('http');
var https = require('https');
var contentType = require('content-type');



/********************************************************************************************
 *  Plugin Object hooks.
 ********************************************************************************************/


exports.factory = function (settings, app, microservice) {

    var that = {};

    that.tokens = settings;
    that.ms = microservice;
    that.webroot = that.tokens.webroot;

    app.use(that.tokens.anchor, handleDocumentRequest)


    // maps file extention to MIME types
    const mimeType = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword',
    '.eot': 'appliaction/vnd.ms-fontobject',
    '.ttf': 'aplication/font-sfnt'
    };    

    /**************************************************************
    * Document Request Handler.
    **************************************************************/

    function handleDocumentRequest(req, res) 
    {
        //Configuration option can force access to 
        //pages provide an API key.
        if( that.tokens.RequireApiKey)
        {
            let api_key = that.ms.getAPIKEY(req);
            if (!api_key) {
                console.error(`\nFailed ${that.tokens.anchor}: x-api-key not defined`);
                RespondCode(resp, 400, -1, "Missing x-api-key.");
                return;
            }
        }        

         
        // parse URL
        const parsedUrl = url.parse(req.url);
        
        // extract URL path
        // Avoid https://en.wikipedia.org/wiki/Directory_traversal_attack
        // e.g curl --path-as-is http://localhost:9000/../fileInDanger.txt
        // by limiting the path to current directory only
        const sanitizePath = path.normalize(parsedUrl.pathname).replace(/^(\.\.[\/\\])+/, '');
        let pathname = path.join(that.webroot, sanitizePath);
        
        fs.exists(pathname, function (exist) {
            if(!exist) 
            {
                // if the file is not found, return 404
                res.statusCode = 404;
                res.end(`File ${pathname} not found!`);
                return;
            }
        
            // if is a directory, then look for index.html
            if (fs.statSync(pathname).isDirectory()) 
            {
                pathname += '/index.html';
            }
        
            // read file from file system
            fs.readFile(pathname, function(err, data)
            {
                if(err){
                    res.statusCode = 500;
                    res.end(`Error getting the file: ${err}.`);
                } else {
                    // based on the URL path, extract the file extention. e.g. .js, .doc, ...
                    const ext = path.parse(pathname).ext;
                    // if the file is found, set Content-type and send data
                    res.setHeader('Content-type', mimeType[ext] || 'text/plain' );
                    res.end(data);
                }
            });
        });
    }

return (that);
}

// This runs until it is killed
console.log("Running ...");
 
