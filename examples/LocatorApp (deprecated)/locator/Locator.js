'use strict';
/* 45678901234567890123456789012345678901234567890123456789012345678901234567890
 *
 * NAME: Locator
 *
 * database : path to SQLite3 database
 * anchor
 * submit
 */

var util = require('util');
var chalk = require('chalk');
var contentType = require('content-type');
var express = require('express');
var bodyParser = require('body-parser');


// TODO need tp [eriodically call "PRAGMA optimize" on SQLite

//var sqlite3 = require('sqlite3');
var sqlite3 = require('sqlite3').verbose();


chalk.enabled = true;

exports.factory = function (settings, app, microservice) {
	var that = {};

	that.tokens = settings;
	that.ms = microservice;

	that.db = new sqlite3.cached.Database(settings.database);
	that.db.run("PRAGMA foreign_keys = ON");
	if (that.tokens.database_init) that.db.exec(that.tokens.database_init);


	that.insertLocateRecord = function (locrec, ilrCallback) {

		console.log("insertLocatRecord:");
		console.log(util.inspect(locrec, { showHidden: false, depth: null }));

		let db = that.db;
		let dra = (new Date()).toISOString();
		try {

			// TODO should check all manditory elements are good

			db.run(
				"INSERT INTO LocateRecord (tid,datetime,devid,_api$key_,acctid,service,lat,lon,alt,radius,vert,azimuth,speed,confidence,dra) VALUES($tid,$datetime,$devid,$appid,$acctid,$service,$lat,$lon,$alt,$radius,$vert,$azimuth,$speed,$confidence,$dra)",
				{
					$tid: locrec.tid,
					$datetime: locrec.date,
					$devid: locrec.devid,
					$appid: locrec.appid,
					$acctid: locrec.acctid,
					$service: locrec.service,
					$lat: locrec.lat,
					$lon: locrec.lon,
					$alt: locrec.alt,
					$radius: locrec.radius,
					$vert: locrec.vert,
					$azimuth: locrec.azimuth,
					$speed: locrec.speed,
					$confidence: locrec.confidence,
					$dra: dra
				},
				function (err) {
					if (err) {
						console.error("INSERT LocateRecord failed: ", err)
						ilrCallback(err);
						return;
					}

					let locrecid = this.lastID;

					if (locrec.attribute) {

						let attr = locrec.attribute;
						let stmt = db.prepare("INSERT INTO LocateRecordAttr (locrecid,name,value) VALUES($locrecid,$name,$value)");

						for (let i = 0, cnt = attr.length; i < cnt; ++i) {
							stmt.run({
								$locrecid: locrecid,
								$name: attr[i].name,
								$value: attr[i].value
							});

						}

						stmt.finalize(function (err) {
							if (err) {
								console.error("INSERT LocateRecordAttr failed: ", err)

								console.error(err);
							}
						}
						);
					}
                    ilrCallback();
				}
			);

		}
		catch (e) {
			ilrCallback(e);
		}
	};


	that.handleQuery = function (req, resp) {
		let api_key = that.ms.getAPIKEY(req);
		if (!api_key) {
			console.error(`\nFailed ${that.tokens.anchor}/${that.tokens.path_query}: no api_key`);
			that.ms.RespondCode(resp, 400, -1, "Missing x-api-key.");
			return;
		}

		console.log(chalk.blue("\nGot Locator/Query: ") + api_key);

		let content_type = req.get('content-type');
		let ct = contentType.parse(content_type);

		if (ct.type != "application/json") {
			console.error(`\nFailed ${that.tokens.anchor}/${that.tokens.path_query}: %s | Got %s expected 'application/json'`, api_key, ct.type);
			that.ms.RespondCode(resp, 415, -1, "Expected 'application/json'.");
			return;
		}

		try {
			//console.log(chalk.green("\nSuccess CAP/Submit: ") + api_key);
			//resp.sendStatus(200);

			let criteria = req.body;

			console.log(util.inspect(criteria, { showHidden: false, depth: null }));


			let from = "LocateRecord l";
			let clause = [];
			let where;

			where = util.format("_api$key_ = '%s'", api_key);
			if (criteria.hasOwnProperty('locrec')) {
				where += " AND " + criteria.locrec;
			}
			clause.push(where);

			if (criteria.hasOwnProperty('attr')) {
				from += ",LocateRecordAttr a";
				clause.push("l.locrecid = a.locrecid AND " + criteria.attr);
			}


			where = clause.shift();
			clause.forEach(function (w) { where += " AND " + w; });

			let doNV = true;
			let doCount = false;

			let fields = "l.locrecid,tid,datetime,devid,acctid,service,lat,lon,alt,radius,vert,azimuth,speed,confidence";
			if (criteria.hasOwnProperty('return')) {
				fields = 'l.locrecid,' + criteria.return.toUpperCase().replace(/FROM/g, '');
				if (fields.startsWith('l.locrecid,COUNT')) {
					fields = 'count(l.locrecid) as count';
					doNV = false;
					doCount = true;
				}
			}

			let limit = criteria.hasOwnProperty('limit') ? criteria.limit : "";

			let sql = "SELECT DISTINCT " + fields + " FROM " + from + " WHERE " + where + " " + limit;
			sql = sql
				.replace(/\!=/g, ' $NOTEQUAL$ ')
				.replace(/\!/g, ' NOT ')
				.replace(/\$NOTEQUAL\$/g, ' != ')
				.replace(/\&\&/g, ' AND ')
				.replace(/\|\|/g, ' OR ')
				.replace(/[*;]/g, '');

			console.log(sql + "\n");


			// now setup the Attribute query

			where = "locrecid = $locrecid";
			if (doNV && criteria.hasOwnProperty('returnAttr')) {
				if (criteria.returnAttr.toLowerCase() == "none")
					doNV = false;
				else {
					let returnAttr = criteria.returnAttr.replace(/[*;]/g, '');
					if (returnAttr.length != 0) {
						let doNOT = returnAttr.charAt(0) == '!';
						if (doNOT)
							returnAttr = returnAttr.substring(1);

						if (returnAttr.length != 0) {
							where += " AND name " + (doNOT ? "NOT " : "") + "IN (" + returnAttr + ")";
						}
					}
				}
			}
			let nvSQL = "SELECT name,value FROM LocateRecordAttr WHERE " + where;
			if (doNV) console.log(nvSQL + "\n");

			resp.setHeader('Content-Type', 'application/json');
			resp.setHeader('Access-Control-Allow-Origin', '*');
			resp.status(200);


			resp.write('{ "record" : ['); //}

			let writeComma = false;
			let lastI = 0;
			let completeI = 0;

			//console.log("starting query for each LocRec....");

			that.db.each(sql,
				function (err, row) {
					//console.log("In LocRec each handler");
					if (err) {
						//console.log(chalk.red("\nSQL Locator/Query: ") + api_key);
						console.error(err);
						if (doNV) {
							++completeI;
							//console.log("completeI = %d", completeI);
							if (completeI == lastI) {
								/*{*/ resp.end('],"code":2}');
							}
						}

						return;
					}


					if (doNV) {
						//console.log("query NV for %s",row.locrecid);
						that.db.all(nvSQL, { $locrecid: row.locrecid },
							function (err, nvs) {
								//console.log("In nv all handler for %s", row.locrecid);

								delete row.locrecid;
								if (err == null) {
									if (nvs.length != 0) {
										row.attribute = nvs;
									}
								}
								else {
									console.error("NameValue: ", err);
								}

								if (writeComma) { resp.write(','); }
								else writeComma = true;
								resp.write(JSON.stringify(row));

								++completeI;
								//console.log("completeI = %d", completeI);


								if (completeI == lastI) {
									/*{*/ resp.end('],"code":1}');
								}
							});

					}
					else {
						delete row.locrecid;
						if (writeComma) { resp.write(','); }
						else writeComma = true;
						resp.write(JSON.stringify(row));
					}
				},
				function (err, rowCnt) {
					// this is the complete function
					//console.log("In LocRec complete handler");

					if (err) {
						//console.log(chalk.red(`\nSQL ${that.tokens.anchor}/${that.tokens.path_query}: `) + api_key);
						console.error(err);
						/*{*/ resp.end('],"code":-1}');
					}
					else {
						if (doNV && rowCnt != 0) {

							lastI = rowCnt; // let NV query finish it up
							//console.log("lastI = %d", lastI);
							if (completeI == lastI) {
								/*{*/ resp.end('],"code":1}');
							}
						}
						else {
							/*{*/ resp.end('],"code":1}');
						}
					}

				}
			);
		}
		catch (e) {
			console.log(chalk.red(`\nFailed ${that.tokens.anchor}/${that.tokens.path_query}: `) + api_key);
			console.log(e);
			that.ms.RespondCode(resp, 500, -1, "failed to query data.");
		}
	};

	that.handlePurge = function (req, resp) {
		let api_key = that.ms.getAPIKEY(req, null);
		if (!api_key) {
			console.error(`\nFailed ${that.tokens.anchor}/${that.tokens.path_purge}: no api_key`);
			that.ms.RespondCode(resp, 400, -1, "Missing x-api-key.");
			return;
		}

		console.log(chalk.blue(`\nGot ${that.tokens.anchor}/${that.tokens.path_purge}: `) + api_key);

		let content_type = req.get('content-type');
		let ct = contentType.parse(content_type);

		if (ct.type != "application/json") {
			console.error(`\nFailed ${that.tokens.anchor}/${that.tokens.path_purge}: %s | Got %s expected 'application/json'`, api_key, ct.type);
			that.ms.RespondCode(resp, 415, -1, "Expected 'application/json'.");
			return;
		}

		if (!req.body.datetime) {
			console.error(`\nFailed ${that.tokens.anchor}/${that.tokens.path_purge}: missing datetime`, api_key, ct.type);
			that.ms.RespondCode(resp, 400, -1, "missing datetime.");
			return;
		}

		try {
			let msg = req.body;

			console.log(util.inspect(msg, { showHidden: false, depth: null }));

			that.db.all("SELECT locrecid FROM LocateRecord WHERE _api$key_ = $appid AND datetime <= $datetime",
				{
					$appid: api_key,
					$datetime: msg.datetime
				},
				function (err, ids) {

					if (err) {
						console.log(chalk.red(`\nFailed ${that.tokens.anchor}/${that.tokens.path_purge}: `) + api_key);
						console.log(e);
						that.ms.RespondCode(resp, 400, -1, e);
					}

					let dellocrec = that.db.prepare("DELETE FROM LocateRecord WHERE locrecid = $locrecid");

					for (let i = 0, cnt = ids.length; i < cnt; ++i) {
						dellocrec.run({ $locrecid: ids[i].locrecid });
					}

					dellocrec.finalize(function (err) {
						if (err) {
							console.error("DELETE LocateRecord failed: ", err)
							console.error(err);
						}
					});
					that.ms.RespondCode(resp, 200, 1);
				}
			);


		}
		catch (e) {
			console.log(chalk.red(`\nFailed ${that.tokens.anchor}/${that.tokens.path_purge}: `) + api_key);
			console.log(e);
			that.ms.RespondCode(resp, 500, -1, "failed to query data.");
		}
	};




	that.handleSubmit = function (req, resp) {
		let api_key = that.ms.getAPIKEY(req, null);
		if (!api_key) {
			console.error(`\nFailed ${that.tokens.anchor}/${that.tokens.path_submit}: no api_key` + api_key);
			that.ms.RespondCode(resp, 400, -1, "Missing x-api-key.");
			return;
		}

		console.log(chalk.blue(`\nGot ${that.tokens.anchor}/${that.tokens.path_submit}: `) + api_key);

		let content_type = req.get('content-type');
		let ct = contentType.parse(content_type);

		if (ct.type != "application/json") {
			console.error(`\nFailed ${that.tokens.anchor}/${that.tokens.path_submit}: %s | Got %s expected 'application/json'`, api_key, ct.type);

			that.ms.RespondCode(resp, 415, -1, "Expected 'application/json'.");
			return;
		}

		try {
			//console.log(chalk.green("\nSuccess Callback/Submit: ") + api_key);
			//resp.sendStatus(200);

			console.log(util.inspect(req.body, { showHidden: false, depth: null }));

			that.insertLocateRecord(req.body, function (err) {
				if (err) {
					console.log(chalk.red(`\nFailed ${that.tokens.anchor}/${that.tokens.path_submit}: `) + api_key);
					console.log(e);
					that.ms.RespondCode(resp, 400, -1, e);
				}
				else {
					that.ms.RespondCode(resp, 200, 1);
				}
			});

		}
		catch (e) {
			console.log(chalk.red(`\nFailed ${that.tokens.anchor}/${that.tokens.path_submit}: `) + api_key);
			console.log(e);
			that.ms.RespondCode(resp, 500, -1, "failed to capture data");
		}
	};


	let router = express.Router();
	router.use(bodyParser.json());

	// finish adding in express app handlers
	router.post(that.tokens.path_purge, that.handlePurge);
	router.post(that.tokens.path_submit, that.handleSubmit);
	router.post(that.tokens.path_query, that.handleQuery);

	app.use(that.tokens.anchor, router)

	return (that);
};


// This runs until it is killed
console.log("Running ...");
