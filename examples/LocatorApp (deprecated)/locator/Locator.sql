-- application must set these PRAGMA also
PRAGMA foreign_keys = ON;
PRAGMA synchronous = 0;
PRAGMA threads = 3;

DROP TABLE IF EXISTS LocateRecord;
DROP TABLE IF EXISTS LocateRecordAttr;

CREATE TABLE IF NOT EXISTS LocateRecord
(
	locrecid INTEGER PRIMARY KEY ASC, -- alias for rowid/oid/_rowid_
	tid TEXT NOT NULL, -- UUID 123e4567-e89b-12d3-a456-426655440000
	datetime TEXT NOT NULL, -- ISO8601
	devid TEXT NOT NULL, -- deviceID
	_api$key_ TEXT NOT NULL, -- APIKEY
	acctid TEXT , -- null allowed Account information
	service TEXT NOT NULL, -- uri of service source
	lat REAL NOT NULL CHECK(lat >= -90.0 AND lat <= 90.0), -- WGS84
	lon REAL NOT NULL CHECK(lon >= -180.0 AND lon <= 180.0), -- WGS84
	alt REAL , -- WGS84
	radius REAL  CHECK(radius >= 0.0), -- meters
	vert REAL  CHECK(vert >= 0.0), -- meters
	azimuth REAL  CHECK(azimuth >= 0 AND azimuth <= 360),  -- degrees
	speed REAL , -- units ?
	confidence REAL NOT NULL CHECK(confidence >= 0 AND confidence <= 1.0),
	dra timestamp DEFAULT CUURRENT_TIMESTAMP
);
CREATE UNIQUE INDEX IF NOT EXISTS LocateRecord_tid_idx ON LocateRecord(tid);
CREATE INDEX IF NOT EXISTS LocateRecord_appid_idx ON LocateRecord(_api$key_);
CREATE INDEX IF NOT EXISTS LocateRecord_datetime_idx ON LocateRecord(datetime);

CREATE TABLE IF NOT EXISTS LocateRecordAttr
(
	-- hidden rowid/oid/_rowid_ is INTEGER PRIMARY KEY
	locrecid INTEGER NOT NULL,
	name TEXT NOT NULL CHECK(length(name) > 0),
	value TEXT NOT NULL,
	FOREIGN KEY(locrecid) REFERENCES LocateRecord(locrecid) ON DELETE CASCADE
);
CREATE INDEX IF NOT EXISTS LocateRecordAttr_locrecid_idx ON LocateRecordAttr(locrecid);
CREATE INDEX IF NOT EXISTS LocateRecordAttr_namevalue_idx ON LocateRecordAttr(name,value);

-- DROP TABLE IF EXISTS Spatial;
-- CREATE VIRTUAL TABLE IF NOT EXISTS Spatial USING rtree(
-- 	locrecid, -- cannot declare a FOREIGN REF but can use a trigger
-- 	latMin, latMax,
-- 	lonMin, lonMax,
-- 	altMin, altMax,
-- 	tmBegin, tmEnd
-- );

