echo "\nKiata Demo Microservices Verification."

echo "\n\n*** Port 8010 APIs "

echo "\n\nLocator Demo Web App"
curl http://localhost:8010/LocatorApp
curl http://localhost:8010/LocatorApp/css/style.css


echo "\n\nLocator Service Linkware Submission (linkware/submit) API"
curl \
  http://localhost:8010/linkware/submit \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'x-api-key: localhost_verify' \
  -d '{"mac": "flex_test1",  "appeui": "2c26c50124194000", "last_update_time": "2019-08-13T00:00:00.000000Z",  "data_type": "2", "data": "0181a1e14910ee010efcecda84ce7a3719918221753644", "reserver": {"code": 0}}'

echo "\n\nLocator Service Query (Locator/query) API"
curl \
  http://localhost:8010/Locator/query \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'x-api-key: localhost_verify' \
  -d '{ "locrec" : "datetime >= \"1900\" order by datetime DESC LIMIT 5"}'

