Source is from `sqlite-amalgamation-3180000`  
Info: <https://www.sqlite.org/download.html>  
Direct: <https://www.sqlite.org/2017/sqlite-amalgamation-3180000.zip>

SQLite is public domain:
<https://www.sqlite.org/copyright.html>

<https://www.sqlite.org/howtocompile.html>

Linux:

~~~shell
gcc -I.  -DSQLITE_ENABLE_JSON1 \
   -DSQLITE_ENABLE_RTREE \
   -DHAVE_USLEEP -DHAVE_READLINE \
   shell.c sqlite3.c -ldl -lreadline -lncurses -lpthread -o sqlite3
~~~

~~~shell
gcc -Os -I. -DSQLITE_THREADSAFE=0 -DSQLITE_ENABLE_FTS4 \
   -DSQLITE_ENABLE_FTS5 -DSQLITE_ENABLE_JSON1 \
   -DSQLITE_ENABLE_RTREE -DSQLITE_ENABLE_EXPLAIN_COMMENTS \ 
   -DHAVE_USLEEP -DHAVE_READLINE \
   shell.c sqlite3.c -ldl -lreadline -lncurses -o sqlite3
~~~

For MacOS, just use `clang` instead of `gcc`
