'use strict';



class DeviceEntry {
    constructor(appid, id) {
    
        this.body = {
            confidence: 0.6826890110969543, //Returns 1-std deviation confidence.
            etid:  appid,
            devid: id,
            typeShape: [ 31 ], //Returns a Spheroid 3-D shape.
            upload : []                
        };
    }
    addPayloadData( buffer, dtTransport ) {
        
        var sEncData = buffer.toString('base64')
        var payload =
        {
            dateTransport: dtTransport,
            gateways: [],
            payload: sEncData
        };        
        this.body.upload.push(payload);
    }
};

module.exports = DeviceEntry;
