'use strict';
// 45678901234567890123456789012345678901234567890123456789012345678901234567890
//
// NAME: linkwareadaptor.js
//
// =============================================================================

var chalk = require('chalk');
chalk.enabled = true;

var util = require('util');
var path = require('path');
var fs = require('fs');
var request = require('request');
var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var https = require('https');
var contentType = require('content-type');

var DeviceEntry = require("./DeviceEntry");

const uuidV4 = require('uuid/v4');
var sqlite3 = require('sqlite3').verbose();

var mapDevices= new Map();

/********************************************************************************************
 *  Plugin Object hooks.
 ********************************************************************************************/


exports.factory = function (settings, app, microservice) {

    var that = {};

    that.tokens = settings;
    that.ms = microservice;

    that.db = new sqlite3.cached.Database(settings.database);
    that.db.run("PRAGMA foreign_keys = ON");
    if (that.tokens.database_init) that.db.exec(that.tokens.database_init);

    that.insertLocateRecord = function (locrec, ilrCallback) {

        console.log("insertLocatRecord:");
        console.log(util.inspect(locrec, { showHidden: false, depth: null }));

        let db = that.db;
        let dra = (new Date()).toISOString();
        try {

            // TODO should check all manditory elements are good

            db.run(
                "INSERT INTO LocateRecord (tid,datetime,devid,_api$key_,acctid,service,lat,lon,alt,radius,vert,azimuth,speed,confidence,dra) VALUES($tid,$datetime,$devid,$appid,$acctid,$service,$lat,$lon,$alt,$radius,$vert,$azimuth,$speed,$confidence,$dra)",
                {
                    $tid: locrec.tid,
                    $datetime: locrec.date,
                    $devid: locrec.devid,
                    $appid: locrec.appid,
                    $acctid: locrec.acctid,
                    $service: locrec.service,
                    $lat: locrec.lat,
                    $lon: locrec.lon,
                    $alt: locrec.alt,
                    $radius: locrec.radius,
                    $vert: locrec.vert,
                    $azimuth: locrec.azimuth,
                    $speed: locrec.speed,
                    $confidence: locrec.confidence,
                    $dra: dra
                },
                function (err) {
                    if (err) {
                        console.error("INSERT LocateRecord failed: ", err)
                        ilrCallback(err);
                        return;
                    }

                    let locrecid = this.lastID;

                    if (locrec.attribute) {

                        let attr = locrec.attribute;
                        let stmt = db.prepare("INSERT INTO LocateRecordAttr (locrecid,name,value) VALUES($locrecid,$name,$value)");

                        for (let i = 0, cnt = attr.length; i < cnt; ++i) {
                            stmt.run({
                                $locrecid: locrecid,
                                $name: attr[i].name,
                                $value: attr[i].value
                            });

                        }

                        stmt.finalize(function (err) {
                                if (err) {
                                    console.error("INSERT LocateRecordAttr failed: ", err)

                                    console.error(err);
                                }
                            }
                        );
                    }
                    ilrCallback();
                }
            );

        }
        catch (e) {
            ilrCallback(e);
        }
    };

    let router = express.Router();

    router.use(bodyParser.json({ type: "application/json", limit: "20kb"}));

    // finish adding in express app handlers
    router.post(that.tokens.path_submit, handleLinkwarePush);
    app.use(that.tokens.anchor, router)

/**************************************************************
*
* Linwkare Push related
*
**************************************************************/

    function handleLinkwarePush(req, resp) {
		let api_key = that.ms.getAPIKEY(req);
		if (!api_key) {
			console.error(`\nFailed ${that.tokens.anchor}/${that.tokens.path_query}: x-api-key not defined`);
			that.ms.RespondCode(resp, 400, -1, "Missing x-api-key in header or query.");
			return;
		}        

        // just be done with it
        console.log(chalk.blue("\nGot Linkware Push Message: ") + req.method);
        console.log(util.inspect(req.headers, {showHidden: false, depth: null}));
        console.log("\n");

        try {
            var length = req.get('content-length');
            if( length == undefined || length == 0)
            {
                resp.status(406).send('No data provided.');
                return;
            }

            // var content_type = req.get('content-type');
            // if( content_type != "application/json")
            // {
            //     resp.sendStatus(415);
            //     console.log(chalk.red("unsupported content-type:"));
            //     var ct = contentType.parse(content_type);
            //     console.log(util.inspect(ct, {showHidden: false, depth: null}));
            //     return;
            // }

            //Parse the body converting received data into one or more messages for Kiata Translator.
            console.log(util.inspect(req.body, {showHidden: false, depth: null}));
            var body =  req.body;
            console.log(chalk.green("\nRecieved Message:"));

            var d = new Date();
            var isoNow = d.toISOString();
            var mapDevices = new Map();

            if( Array.isArray(body))
            {
                resp.sendStatus(200);
                console.log(chalk.green("Linkware Push Multiple: " + req.method + " -> 200 - OK"));

                for( const  msg of body)
                {
                    var akey= (api_key=="UseAppKey") ? msg.appeui : api_key;

                    if( parseInt(msg.data_type, 10) == 2)
                    {
                        AddMessage( mapDevices, akey, msg.mac, msg.data, isoNow);
                    }                    
                }
                console.log("Received multiple entries.");
            }
            else if (body.hasOwnProperty('mac') && body.hasOwnProperty('data_type') && body.hasOwnProperty('data'))
            {
                resp.sendStatus(200);
                console.log(chalk.green("Linkware Push Single: " + req.method + " -> 200 - OK"));

                var akey= (api_key=="UseAppKey") ? body.appeui : api_key;


                //Only parse messages.
                var data_type = parseInt(body.data_type, 10);
                if( data_type  == 2)
                {
                    AddMessage( mapDevices, akey, body.mac, body.data, isoNow);
                }
            }
            else
            {
                resp.sendStatus(400);
                console.log(chalk.green("LinkwarePush: " + req.method + " -> 400 - Bad Data"));
            }

            if( mapDevices.size > 0)
            {
                //Iterates map, posting all the data to the translation service.
                //We will process the responses sometime later.
                for( var[key, entry] of mapDevices)
                {
                    PostReceivedDeviceData( entry.body);
                }
            }
        }
        catch (e) {
            console.error("handleLinkwarePush " + req.method + " catch: ", e);
            if (!resp.headersSent) {
                resp.status(500).send('Something went wrong.');
            }
        }
    }

    // -------------------------------------------
    //
    // Processes received linkware message data.
    // Organizes data by device in preparation for transmittle to Kiata 
    // Translation Decode service.
    //
    // -------------------------------------------
    function AddMessage( mapDevices, appid, devid, data, dtTransport)
    {
        var entry = mapDevices.get(devid);

        if( entry == null || entry == undefined)
        {
            entry = new DeviceEntry( appid, devid);
            mapDevices.set( devid, entry);
        }
        
        //Linkware publishes data as hex.
        var buffer = Buffer.    from( data, 'hex');

        entry.addPayloadData(buffer, dtTransport);
    
    }

    // -------------------------------------------
    //
    // Posts JSON Body Data to Kiata Translate Decode API.
    //
    // -------------------------------------------
    function PostReceivedDeviceData( body )
    {
        var optionDecode =
            {
                url: settings.urlDecode,
                method: "POST",
                headers: {
                    'x-api-key': settings.apiKey,
                    'Content-Type':  'application/json',
                    'Accept': 'application/json'
                },
                body : JSON.stringify(body)
            };

        // log what is about to be sent:
        console.log(chalk.magenta("\nSend Kiata Translation decode body:"));
        console.log(util.inspect(optionDecode, {showHidden: false, depth: null}));

        request(optionDecode, handleDecodeResponse);
    }

    // -------------------------------------------
    //
    // Shared Code
    //
    // -------------------------------------------
    //

    function handleDecodeResponse(error, response, body) {
        if (error) {
            console.error("Kiata Translation Decode failed: ", error);
            return;
        }
        if (response.statusCode != 200) {
            console.error("Kiata Translation Decode failed with: %d\n", response.statusCode);
            return;
        }

        try {
            var resp = JSON.parse(body);
            console.log(chalk.green("\nReceived Kiata Translation Decode Response body:"));
            console.log(util.inspect(resp, {showHidden: false, depth: null}));

            //Get identifiers for device and application 
            var idDevice = "UNKNOWN";
            var idApp = "UNKNOWN";
            var self = that;

            if (resp.hasOwnProperty('devid'))
                idDevice = resp.devid;
            if (resp.hasOwnProperty('etid'))
                idApp = resp.etid;

            //Iterate if there are messages.
            if (resp.hasOwnProperty('messages') && resp.messages.length > 0) {
                resp.messages.forEach( function( msg){

                    //Process new Location Reports only. We support only shape type
                    //31, which is a Spheroid.
                    if( msg.msgtype == "LocationReport" && msg.updated == true)
                    {
                        //if(  msg.hasOwnProperty('location') && msg.location.hasOwnProperty('shape')
                    //&& msg.location.shape.shapetype == 31 )
                    
                        var spheroid = msg.location.shape;
                        let locrec = {
                            tid: uuidV4(),
                            date: msg.epoch,
                            devid: idDevice,
                            appid: idApp,
                            service: 'micros://linkware',
                            lat: spheroid.lat,
                            lon: spheroid.lon,
                            alt: spheroid.alt,
                            radius: spheroid.radius,
                            vert: spheroid.vert,
                            confidence: msg.location.confidence
                        };

                        //TODO add support for floor and speed.
                        //if( shape.hasOwnProperty('floor') ) 
                        //    locrec.floor = msg.location.floor;

                        //Insert the record into locator database.
                        self.insertLocateRecord(locrec, function (e) { 
                            if (e) console.error(e); 
                        });
                    }
                    else{
                        //TODO Log Int32, KeyValueMsg, and UnencodedMsg messages into database.
                        console.log(chalk.magenta("\n* Unhandled message for device: " + idDevice +", msgtype: " + msg.msgtype));
                    }
                });
            }
        }
        catch (e) {
            console.error("handleDecodeResponse catch: ", e);
        }
    }



return (that);
}

// This runs until it is killed
console.log("Running ...");
 
