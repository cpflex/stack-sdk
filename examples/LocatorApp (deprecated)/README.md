# Locator App Example  

This Node JS application implements a simple web tracking application using the CP-Flex translation API.
It serves as an example on how to interact with the CP-Flex translation APIs as well as receiving data from Easy Linkin Linkware services.

This is a bundled  cloud service comprising

* MicroService framework
* Linkware / Translation Adaptor
* LocatorDB microservice module
* Static web page server (WebServer) providing html and css files.

This application can also be packaged as a docker container for easy deployment. The following
links provide additional information.
  
1) Packaging Instructions see [Docker Deployment](#Docker-Deployment) section below.  
2) Live Deployment  see [Reference Docker Deployment](#Reference-Docker-Deployment)

## Prerequisite

* node.js is installed
* gcc required for sqlite module

## Installation

From the directory you found this README.md:

~~~bash
npm install
~~~

This will create a `node_modules` directory, pull down all the required modules and for the SQLite module, you will likely see it getting recompiled.

## Configuration

Configuraiton can be found in `locatorservice.json`  
The configuration file is documented with JSON, see `"comment-*"`

### path configuration

For each microservice, there is an `anchor` and a set of `path_*` that are added to the `anchor`.  You can change all anchors and paths through configuration.

For the MicroService framework, there are two special `path_` settings:

* `path_status` (usually "/status") will return result-code 200
* `path_log` (usually "/log") will return HTML to watch the log

These MicroService paths can be disabled by removing their `path_` configuration or emptying the value (e.g. `""`)

## SQLite DB
linkwareadaptor.js and Locator.js use sqlite3 and this is taken care of by npm.

However, the database they use needs to be created. A version of sqlite is checked-in in the `sqlite` directory.  
Take a look at its [README.md](sqlite/README.md) on how to build.

Once you have `sqlite3` do the following command to create the database:

~~~shell
cat locator/Locator.sql | ./sqlite/sqlite3 locator.db
~~~

## Executing
With locatorservice.json configured and locator.db created, now just execute the microservices:

~~~shell
./locatorservice.js
~~~

OR

~~~shell
./keepAlive ./locatorservice.js >& locatorservice.out &
~~~

---

# Docker Deployment


Before building the package, please make sure the project version is properly tagged.   If you issue the following command you can
The version of the package is defined in  env_container.bash file.   Roll the version 
as needed.   

export LOCATOR_VERSION=v1.0.0

Ideally the package should always be set to a specific tag before building so you get a clean version. 
Use the git tag command to do this. For example:

```
git tag v1.0.0.1234
```

Before generating the tag, update the LOCATOR_VERSION definition in env_container.bash with the specified version
Otherwise you may not be able to synch versions later.   In general, developers using the 
base release without modification will not have to modify these values.

The latest tag in git will become part of the name of the docker image.  Using the above example version the tar file will be named:

```
package_demo_locator.v1.0.0.1234.tar 
```

To build the docker image:
```
    ./docker/build.sh.
```

This will create generate package_demo_locator.v1.0.0.1234.tar which can now 
be used to launch the locator demo service.  The tar file can be save for 
other deployments without having to recreate the image.

To load the docker image, first verify it is already loaded into docker.

```
docker images
```

if not, load the image. 

```
docker load < package_demo_locator.v1.0.1234.tar
```

Now you are ready to run the stack.  Three shell scripts are available to manage the stack.  To bring the stack up, use:

```
./scripts/up_stack.sh
```

Once the stack is up and use the following command to verify the APIs are functioning:

```
./scripts/verify_local.sh
```

To bring down the stack use:

```
./scripts/down_stack.sh
```

---

# Reference Docker Deployment
Users may access the standard deployment of the LocatorApp demo application that is
provided here.  To access the Locator web applcation go to 
http://test.codepoint.services/cpflex/demo/locator/v1.0/app/index.html.  Clicking 
this link starts a web browser based session.

The underlying web services are:

1. Linkware Submission: http://test.codepoint.services/cpflex/demo/locator/v1.0/linkware/submit
2. Locator Query: http://test.codepoint.services/cpflex/demo/locator/v1.0/query

These APIs are best understood using the associated Postman's, which excerise the interface.

The WebApp is the end user application and demonstrates how to access data managed by the LocatorApp 
web services.   It requires that the user specify an Account Name/api-key that matches the x-api-key used
when submitting data via the linkware submission function.   See the LocatorApp for additional
details on using these services.  The basic app is provided as a means to verify your own 
services and implementations.  It is not intended for commercial use.   


---
*Copyright 2019-2020, Codepoint Technologies, Inc.* 
*All Rights Reserved*
