'use strict';
// 45678901234567890123456789012345678901234567890123456789012345678901234567890
//
// NAME: MicroService
// =============================================================================

var that = exports;

let util = require('util');
let path = require('path');
let fs = require('fs');
let http = require('http');
let https = require('https');
var express = require('express');
var cors    = require('cors');
var through = require('through');
var chalk = require('chalk');
var ansiHTML = require('ansi-html');
const ansiStrip = require('strip-ansi');

chalk.enabled = true;


// takes an array of config names or JSON, but the first entry must be a
// config name.
function run(args) {

	// createNamespace should only every be called once
	let session = require('continuation-local-storage').createNamespace('Tensor.MicroService');
	exports.session = session


	// read in configs and/or process JSON
	// First entry must be a config name.
	let config = getConfigFile(args[0]);
	if (!config) { return (2); }

	for (let i = 1, cnt = args.length; i < cnt; ++i) {

		let arg = args[i].trim();
		if ( arg.charAt(0) == '{' ) // this is JSON
		{
			try
			{
				let json = JSON.parse(arg);
				mergeObjects(config, json);
			} catch(e)
			{
				console.error(e);
				return(2);
			}
		}
		else
		{
			let cfg2 = getConfigFile(arg);
			if (!cfg2) { return (2); }
			mergeObjects(config, cfg2);
		}
	}

	exports.catify = []; // index with APIKEY to get a "through" object.

	// setup the express application.
	var app = express();
	app.disable('x-powered-by');

	app.use(cors());

	// this middleware adds 'apikey' into every session
	app.use(function (req, res, next) {
		let apikey = getAPIKEY(req, 'global');

		// wrap the events from request and response
		session.bindEmitter(req);
		session.bindEmitter(res);

		// run following middleware in the scope of
		// the namespace we created
		session.run(function () {
			// set apikey on the session, makes it
			// available for all continuations
			session.set('apikey', apikey);
			next();
		});
	});

	if( config.server.path_status && config.server.path_status != "")
		app.get(config.server.path_status, handleStatus);

	if( config.server.path_log && config.server.path_log != "")
		app.get(config.server.path_log, handleLog);

	exports.app = app;

	let plugcnt = 0;

	if (config.plugins && config.plugins instanceof Array) {

//		console.log(util.inspect(config.plugins, { showHidden: false, depth: null }));


		// merge in all configurations
		for (let i = 0, cnt = config.plugins.length; i < cnt; ++i) {
			if (config.plugins[i].config) {
				mergeObjects(config, getConfigFile(config.plugins[i].config));
			}
		}

		// load all the configured plugins
		for (let i = 0, cnt = config.plugins.length; i < cnt; ++i) {
			let plugin = config.plugins[i];

			console.log('Loading Plugin %s using configuration "%s"',
				plugin.module, plugin.props);

			plugin.instance = null;
			try {
				let plugreq = require(plugin.module);
				plugin.instance = plugreq.factory(config[plugin.props], app, exports);
			} catch (e) {
				console.error(e); // SHOULD we EXIT ?
			}
			if (plugin.instance == null) {
				console.error('Plugin %s using configuration "%s" failed to instantiate', plugin.module, plugin.props);
				continue;
			}
			else {
				++plugcnt;
			}
		}
	}

	{
		// setup the clear and SSL servers and associate the express app

		let clearServer = null;
		let sslServer = null;

		if (config.server.SSLport && config.server.SSLport != 0) {
			let options = {
				key: fs.readFileSync(config.server.SSLkey),
				cert: fs.readFileSync(config.server.SSLcert)
			};
			if (config.server.SSLca && config.server.SSLca != null)
				options.ca = fs.readFileSync(config.server.SSLca);

			sslServer = https.createServer(options, app);
			sslServer.listen(config.server.SSLport, config.server.host);
			console.log("HTTPS listening on %s:%d",
				config.server.host, config.server.SSLport);
		}

		if (config.server.port && config.server.port != 0) {
			clearServer = http.createServer(app);
			clearServer.listen(config.server.port, config.server.host);
			console.log("HTTP listening on %s:%d",
				config.server.host, config.server.port);
		}

		exports.clearServer = clearServer;
		exports.sslServer = sslServer;


		// replace the log handlers

		that.origlog = console.log;
		console.log = function () {
			logTo(that.origlog, util.format.apply(util, arguments));
		};

		that.origerror = console.error;
		console.error = function () {
			logTo(that.origerror, chalk.red(util.format.apply(util, arguments)));
		};

		// This runs until it is killed

		console.log("Running ...");
	}

	return(0);
}
exports.run = run;




//
// read the JSON config file
// If there is a 'include' elemenent in the configFile, it will read
// the include in and add to it the rest of the configFile.
// returns object or null
//
function getConfigFile(configFile) {
	let config;
	try {
		config = JSON.parse(fs.readFileSync(configFile, 'utf8'));

		if (config.hasOwnProperty('include')) {
			if (Array.isArray(config.include)) {
				let include = {};
				for (let i = 0; i < config.include.length; ++i) {
					let inc = path.join(path.dirname(configFile), config.include[i]);
					let cfg = getConfigFile(inc);
					include = mergeObjects(include, cfg);
				}
				config = mergeObjects(include, config);
			}
			else {
				let inc = path.join(path.dirname(configFile), config.include);
				let include = getConfigFile(inc);
				config = mergeObjects(include, config);
			}
		}
	}
	catch (e) {
		console.error('getConfigFile Error reading %s', configFile, e);
		config = null;
	}
	return (config);
}
//exports.getConfigFile = getConfigFile;


/*
 * * Recursively merge properties of two objects 
 * */
function mergeObjects(obj1, obj2) {
	for (let p in obj2) {
		try {
			// Property in destination object set; update its value.
			if (obj2[p].constructor == Object) {
				obj1[p] = mergeObjects(obj1[p], obj2[p]);
			} else {
				obj1[p] = obj2[p];
			}
		} catch (e) {
			// Property in destination object not set; create it and set its value.
			obj1[p] = obj2[p];
		}
	}
	return obj1;
}
exports.mergeObjects = mergeObjects;


function getAPIKEY(req, defaultAPIKEY) {
	if (req) {
		// check HTTP header
		let api_key = req.get('x-api-key');
		if (!api_key) {
			// check query-param
			if (req.query['x-api-key'])
				api_key = unescape(req.query['x-api-key']);
			else
				api_key = defaultAPIKEY;
		}

		return (api_key);
	}
	else {
		return (that.session.get('apikey') || 'global');
	}

}
exports.getAPIKEY = getAPIKEY;


function RespondCode(resp, rc, code, str) {
	if (!resp.headersSent) {
		resp.setHeader('Content-Type', 'application/json');
		resp.status(rc);
		if (str)
			resp.end(`{"code":${code},"errinfo":"${str}"}`);
		else
			resp.end(`{"code":${code}}`);
	}
}
exports.RespondCode = RespondCode;


function handleStatus(req, resp) {
	console.log("/status - OK");
	resp.sendStatus(200);
}


function logTo(where, str) {
	let apikey = getAPIKEY();

	if (apikey && that.catify[apikey])
		that.catify[apikey].write(ansiHTML(str));

	str = chalk.gray(apikey) + '|' + str;

	if (that.catify['global'] && ((apikey && apikey != "global") || (!apikey)))
		that.catify['global'].write(ansiHTML(str));

	if (chalk.supportsColor)
		where.call(console, str);
	else
		where.call(console, ansiStrip(str));
}



function handleLog(req, resp) {
	let apikey = getAPIKEY(req, 'global');

	if (!that.catify[apikey]) {
		that.catify[apikey] = new through();
	}

	resp.writeHead(200,
		{
			'Content-Type': 'text/html; charset=utf-8',
			'Transfer-Encoding': 'chunked',
			'X-Content-Type-Options': 'nosniff'
		});
	resp.write('<html><body><script> var gSH = 0; function GoBottom() { if( document.body.scrollHeight != gSH) {gSH = document.body.scrollHeight; window.scrollTo(0,document.body.scrollHeight)}; setTimeout(GoBottom,1000); } GoBottom();</script>\n');
	resp.write("------- " + apikey + "-------<pre>");

	that.catify[apikey].on('data', function (chunk) {
		try {
			resp.write(chunk);
			resp.write('\n');
		}
		catch (e) {
			// TODO, need to disconnected this function.
		}
	}
	);
	resp.on('finish', function () {
		if (apikey != 'global')
			delete that.catify[apikey];
	});
}
