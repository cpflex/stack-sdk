source env_container.bash
docker build -t package_kiata_locator:$LOCATOR_VERSION -f ./docker/Dockerfile .
docker image save -o package_kiata_locator.$LOCATOR_VERSION.tar package_kiata_locator:$LOCATOR_VERSION
