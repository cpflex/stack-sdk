'use strict';
// 45678901234567890123456789012345678901234567890123456789012345678901234567890
//
// This module has common routines across scripts
//

var fs = require('fs');
var path = require('path');

//
// read the JSON config file
// If there is a 'cfgbase' elemenent in the configFile, it will read
// the cfgbase in and add to it the rest of the configFile.
// returns object or null
//
function getConfigFile(configFile) {
  let config;
  try {
    config = JSON.parse(fs.readFileSync(configFile, 'utf8'));

    if (config.hasOwnProperty('cfgbase')) {
      if (Array.isArray(config.cfgbase)) {
        let cfgbase = {};
        for (let i = 0; i < config.cfgbase.length; ++i) {
          let inc = path.join(path.dirname(configFile), config.cfgbase[i]);
          let cfg = getConfigFile(inc);
          cfgbase = mergeObjects(cfgbase, cfg);
        }
        config = mergeObjects(cfgbase, config);
      }
      else {
        let inc = path.join(path.dirname(configFile), config.cfgbase);
        let cfgbase = getConfigFile(inc);
        config = mergeObjects(cfgbase, config);
      }
    }
  }
  catch (e) {
    console.error('getConfigFile Error reading %s', configFile, e);
    config = null;
  }
  return (config);
}
exports.getConfigFile = getConfigFile;


/*
 * * Recursively merge properties of two objects 
 * */
function mergeObjects(obj1, obj2) {
  for (let p in obj2) {
    try {
      // Property in destination object set; update its value.
      if (obj2[p].constructor == Object) {
        obj1[p] = mergeObjects(obj1[p], obj2[p]);

      } else {
        obj1[p] = obj2[p];

      }

    } catch (e) {
      // Property in destination object not set; create it and set its value.
      obj1[p] = obj2[p];

    }
  }

  return obj1;
}
exports.mergeObjects = mergeObjects;


function getAPIKEY(req, defaultAPIKEY) {
  // check HTTP header
  let api_key = req.get('api_key');
  if (!api_key) {
	  api_key = req.get('api-key');
	  if (!api_key) {
		// check query-param
		if (req.query.api_key)
		  api_key = unescape(req.query.api_key);
		else if (req.query['api-key'])
		  api_key = unescape(req.query['api-key']);
		else
		  api_key = defaultAPIKEY;
		}
  }

  return (api_key);
}
exports.getAPIKEY = getAPIKEY;
