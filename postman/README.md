# CP-Flex Cloud Stack Services Postman Collections
The postmans are provided as examples for using the various APIs available with the CP-Flex service.

The collection file requires that the local and cloud environments be loaded prior to using
the collections they define a number of global variables, which set up the postman's to 
communicate either with a locally deployed or cloud deployed instance of the services.

Look in the /Postman folder for these collection and environment files.

To use the Postmans you will need to do the following:

1. Import the collection of interest into Postman.
2. Under "Environments", import the CP-Flex Stack (global.cpflex.tech) environment.
   All the postman's are designed to manage their configurations via environment macros. 
   You must define the FLEX_AUTHKEY value and potentially the FLEX_ACCKEY value in order to access the APIs.

--- 

The following is a brief description of each Postman.

* **CP-Flex Stack(global.cpflex.tech) Environment** -- Environment variables for communicating 
  with the CP-Flex Global Cloud Stack and API documentation.  This is required to use the postman collections.  

* **CP-Flex Provisioning API Postman Collection** -- Collection of postmans for interacting with the 
  Provisioning services APIs.  See also the ```cpacc``` project at https://gitlab.com/cpflex/tools/cpacc and API documentation.

* **CP-Flex Integration API Postman Collection** -- Collection of postmans for interacting with various endpoints
 on the integration service.  See also the integration API documentation.

* **CP-Flex Hub API Postman Collection** -- Collection of APIs for interacting with the Development
  hub.  See also the ```cphub``` project at https://gitlab.com/cpflex/tools/cphub and API documentation.

* **CP-Flex Hub API Postman Collection** -- Collection of APIs for accessing the CP-Flex claims service.  
  See also the ```cpclaim``` project at https://gitlab.com/cpflex/tools/cpclaim and API documentation.


* **CP-Flex Translation APIs (deprecated)**  Collection of APIs demonstrating using the translation APIs
  directly. See also the translation API documentation. This is a low-level API not recommended for new development.





---
*Copyright 2019-2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
